<?php
class hrsz_query_Class  {

    private $query_table = 'fhnpi_local_nyilvtar.fhnpi_local_nyilvtar_hrszalreszlet';
    private $filter_column = 'dat_telepules';
    private $nested_column = 'gis_cimke';
    private $function_name = 'hrsz_query';
    private $id = 'gis_cimke';
    private $box_title = 'Helyrajziszám lekérdezés';


    function print_box ($params='') {
        global $ID;
        $t = new table_row_template();


        $iv = base64_decode($_SESSION['openssl_ivs']['text_filter_table']);
        $encrypted_table_reference = base64_encode(openssl_encrypt($this->query_table, "AES-128-CBC", $_SESSION['private_key'], OPENSSL_RAW_DATA,$iv));

        $t->cell(mb_convert_case($this->box_title, MB_CASE_UPPER, "UTF-8") . ' <i id="hrsz_query_info" class="fa fa-lg fa-question-circle-o" style="float: right; padding-right: 5px;"> </i> ',2,'title center');
        $t->row();

        $s = new select_input_template();
        $s->autocomplete = true;
        $s->access_class = 'qfc nested';
        $s->placeholder = 'Település';
        $s->destination_table = $this->query_table;
        $s->tdata = array('nested_column'=>$this->nested_column,'nested_element'=> $this->nested_column,'etr'=>$encrypted_table_reference,'all_options'=>'on');
        $s->new_menu($this->filter_column,'');

        $t->cell("<input id='select_with_{$this->function_name}_polygon' data-etr='$encrypted_table_reference' class='custom_box' value='$this->function_name' type='hidden'><input type='hidden' class='speedup_filter' value='on'><input type='hidden' id='spatial_function' name='spatial_function' value='within'>$s->menu",2,'content');
        $t->row();
        $t->cell("<select id=$this->id data='custom_polygon_id' class='qfc custom_polygon_id' multiple='multiple' size=6></select>",2,'content');
        $t->row();
        //$t->cell("<button id='select_with_custom_polygon' class='button-gray button-xlarge pure-button' data-etr='$encrypted_table_reference'><i class='fa-search fa'></i> ".str_query."</button>",2,'title');
        $t->row();
        return sprintf("<table class='mapfb'>%s</table>",$t->printOut());

    }

    function print_js ($params='') {
        return '

    if (typeof etr === "undefined")
        var etr = "";

    $(document).ready(function() {
        var cpv_id = $("#select_with_'.$this->function_name.'_polygon").closest(".mapfb").find(".custom_polygon_id").attr("id");
        var '.$this->function_name.'_selected
        $("#'.$this->id.'").multiSelect({
            selectableHeader: \'<input type="text" class="search-input" autocomplete="off" placeholder="search ...">\',
            selectionHeader: \'<input type="text" class="search-input" autocomplete="off" placeholder="search ...">\',
            afterInit: function(ms){
              var that = this,
                  $selectableSearch = that.$selectableUl.prev(),
                  $selectionSearch = that.$selectionUl.prev(),
                  selectableSearchString = "#"+that.$container.attr("id")+" .ms-elem-selectable:not(.ms-selected)",
                  selectionSearchString = "#"+that.$container.attr("id")+" .ms-elem-selection.ms-selected";

              that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
              .on("keydown", function(e){
                if (e.which === 40){
                  that.$selectableUl.focus();
                  return false;
                }
              });

              that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
              .on("keydown", function(e){
                if (e.which == 40){
                  that.$selectionUl.focus();
                  return false;
                }
              });
            },
            afterSelect: function(){
                getPolygon()  
              this.qs1.cache();
              this.qs2.cache();
            },
            afterDeselect: function(){
                getPolygon()
              this.qs1.cache();
              this.qs2.cache();
            }
        });
        async function getPolygon(){
            let polygon_ids = [];
            $("#ms-'. $this->nested_column.' .ms-selection").find(".ms-selected").each(function() {
                polygon_ids.push($(this).data("id"))
            })
            vectorLayer.getSource().clear();
            if (!polygon_ids.length) {
                return
            }
            etr = $("#select_with_'.$this->function_name.'_polygon").data("etr");
            let polygon_wkts = await $.post("ajax", {getWktGeometries: polygon_ids, custom_table:etr});
            polygon_wkts = JSON.parse(polygon_wkts);
            Object.keys(polygon_wkts).forEach(function(k){
                // draw polygon on map
                drawPolygonFromWKT(JSON.stringify(polygon_wkts[k]),1);
                skip_loadQueryMap_customBox = 1;
            });
        }
        $("#mapfilters").on("click", "#'.$this->function_name.'_info", function() {
            $( "#dialog" ).text("'.get_option( $this->function_name . '_info').'");
            $( "#dialog" ).dialog( "open" );
        });
        $("#"+cpv_id).on("nested_data_appended", function () {
            $("#'.$this->id.'").multiSelect("refresh");
        })
    });

    // this function name will be automaticall processed by its  name
    function custom_'.$this->function_name.'() {
        var qids = new Array();
        var qval = new Array();

        $(".qfc").each(function(){
            var stra = new Array();
            var qid=$(this).attr("id");
            if ( $(this).prop("type") == "text" ) {
                stra.push($(this).val());
            } else {
                $("#" + qid + " option:selected").each(function () {
                    stra.push($(this).val());
                });
            }
            if (stra.length) {
                qids.push(qid);
                qval.push(JSON.stringify(stra));
            }
        });
        var asdf = [];
        $("#ms-'. $this->nested_column.' .ms-selection").find(".ms-selected").each(function() {
            asdf.push($(this).text())
        })
        
        var myVar = { geom_selection:"custom_polygons",spatial_function:$("#spatial_function").val(),custom_table:etr }

        for (i=0;i<qids.length;i++) {
            myVar["qids_" + qids[i]] = qval[i];
        }
        myVar["qids_'. $this->nested_column.'"] = JSON.stringify(asdf)

        return myVar;
    }
';

    }
}
?>
