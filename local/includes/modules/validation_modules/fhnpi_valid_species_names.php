<?php
class fhnpi_valid_species_names extends validation_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('fhnpi_valid_species_names initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        $rules = [];
        $cmd = 'select mi_fajered, count(*) c from fhnpi_faj where mi_fajvalid is null GROUP BY mi_fajered ORDER BY c DESC';
        if (!$res = pg_query($ID, $cmd)) {
            job_log("ERROR: query error");
        }
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader(['invalid_value','count']);
        while ($row = pg_fetch_assoc($res)) {
            $tbl->addRows($row);
        }

        return $tbl->printOut();
    }

    static function run() {
        global $ID;

        if (!$res = pg_query($ID, 'WITH rows AS (SELECT DISTINCT f.mi_fajered, foo.fajvalid FROM fhnpi_faj f LEFT JOIN (SELECT t.word as fajered, tt.word as fajvalid FROM fhnpi_taxon t LEFT JOIN fhnpi_taxon tt ON t.taxon_id = tt.taxon_id WHERE t.lang = \'mi_fajered\' AND tt.lang = \'mi_fajvalid\' AND tt.word IS NOT NULL) foo ON f.mi_fajered = foo.fajered WHERE f.mi_fajvalid IS NULL AND foo.fajvalid IS NOT NULL) UPDATE fhnpi_faj ff SET mi_fajvalid = rows.fajvalid FROM rows WHERE rows.mi_fajered = ff.mi_fajered AND ff.mi_fajvalid IS NULL;'))
        {
            job_log(pg_last_error($ID));
            exit;
        }

        $aff_rows = pg_affected_rows($res);
        if ($aff_rows > 0)
            job_log("Validált fajnevek száma: $aff_rows");
        exit;
    }
}
?>
