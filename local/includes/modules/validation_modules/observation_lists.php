<?php
class observation_lists extends validation_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('observation_lists initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        foreach ($params as $table => $options) {
            extract((array)$options);
            $time_conversion = "to_timestamp(%s::bigint/1000)";
            $duration_conversion = "($time_conversion - $time_conversion)::time";
            if (isset($only_time)) {
                $time_conversion .= "::time";
                if (isset($time_as_int)) {
                    $time_conversion = "extract(epoch from date_trunc('minute',$time_conversion))/60::integer";
                    $duration_conversion = "extract(epoch from date_trunc('minute',$duration_conversion))/60::integer";
                }
            }
            
            
            $tmptable = "{$table}_obm_obsl";
            $dst_columns = array_values(array_filter(self::getColumnsOfTables($table), function($col) { return $col != 'obm_id'; }));
            $src_columns = array_values(array_filter(self::getColumnsOfTables($tmptable, 'temporary_tables'), function($col) { return $col != 'obm_id'; }));
            $common_columns = implode(',',array_intersect($src_columns, $dst_columns));

            //get the unprocessed lists
            $lists = self::get_unprocessed_lists( $table );
            if ($lists === 'error') {
                job_log('observation_lists failed 2');
                return;
            }

            // if there are no unprocessed lists
            if ($lists === false) {
                return;
            }

            foreach ($lists as $list) {
                $cmd = [];
                $update_cmd = [];
                $list_id_column = $list_id_column ?? 'obm_observation_list_id';

                if ($list['measurements_num'] > count(explode(',',$list['observation_list_elements']))) {
                    job_log($list['observation_list_id'] . ': list not uploaded completly');
                    continue;
                }

                $update_cmd[] = sprintf("%s = %s", $list_id_column, quote($list['observation_list_id']));
                if (isset($list_start_column)) {
                    $update_cmd[] = sprintf("%s = $time_conversion", $list_start_column, $list['observation_list_start']);
                }
                if (isset($list_end_column)) {
                    $update_cmd[] = sprintf("%s = $time_conversion", $list_end_column, $list['observation_list_end']);
                }
                if (isset($list_duration_column)) {
                    
                    $update_cmd[] = sprintf("%s = $duration_conversion", $list_duration_column, $list['observation_list_end'], $list['observation_list_start']);
                }



                $cmd[] = sprintf("UPDATE temporary_tables.%s SET %s WHERE obm_uploading_id IN (%s);", $tmptable, implode(', ',$update_cmd), $list['observation_list_elements']);

                $cmd[] = sprintf("INSERT INTO %s (%s) SELECT %s FROM temporary_tables.%s WHERE obm_uploading_id IN (%s);",
                            $table,
                            $common_columns,
                            $common_columns,
                            $tmptable,
                            $list['observation_list_elements']
                        );
                $cmd[] = sprintf("INSERT INTO %s_observation_list (oidl, uploading_id, obsstart, obsend) VALUES (%s, %s, to_timestamp(%s::bigint/1000), to_timestamp(%s::bigint/1000));",
                            PROJECTTABLE,
                            quote($list['observation_list_id']),
                            quote($list['uploading_id']),
                            $list['observation_list_start'],
                            $list['observation_list_end']
                );
                $cmd[] = sprintf("UPDATE system.uploadings SET project_table = %s WHERE id IN (%s);", quote($table), $list['observation_list_elements']);
                $cmd[] = sprintf("UPDATE system.uploadings SET project_table = %s WHERE id = %s;", quote($table), quote($list['uploading_id']));

                $cmd[] = sprintf("DELETE FROM temporary_tables.%s WHERE obm_uploading_id IN (%s);", $tmptable, $list['observation_list_elements']);

                if ( parent::query($ID, $cmd) ) {
                    job_log($list['observation_list_id'] . ': list processed');
                }
            }
        }
    }

    private static function get_unprocessed_lists($table) {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table = 'temporary_tables.%2\$s_obm_obsl')
                        SELECT
                            l.id AS uploading_id,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            string_agg(o.id::text,',') as observation_list_elements
                        FROM upl l LEFT JOIN upl o ON l.metadata->>'observation_list_id' = o.metadata->>'observation_list_id'
                        WHERE
                            l.metadata::jsonb ? 'measurements_num' AND
                            NOT o.metadata::jsonb ? 'measurements_num'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        $table
                    );
        if (! $res = parent::query($ID, $cmd) ) {
            return 'error';
        }
        return pg_fetch_all($res[0]);
    }
    /*
    private static function get_unprocessed_null_lists($table) {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table = 'temporary_tables.%2\$s_obm_obsl')
                        SELECT
                            l.id AS uploading_id,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            string_agg(o.id::text,',') as observation_list_elements
                        FROM upl l LEFT JOIN upl o ON l.metadata->>'observation_list_id' = o.metadata->>'observation_list_id'
                        WHERE
                            l.metadata::jsonb ? 'measurements_num' AND
                            NOT o.metadata::jsonb ? 'measurements_num'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        $table
                    );
        if (! $res = parent::query($ID, $cmd) ) {
            return 'error';
        }
        return pg_fetch_all($res[0]);
    }
    */
    private static function getColumnsOfTables($table=PROJECTTABLE,$schema='public') {
        global $GID;
        if ($schema == '') $schema = 'public';
        if ($table == '') $table = PROJECTTABLE;
        $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_name=%s AND table_schema=%s ORDER BY column_name",quote($table),quote($schema));
        $result = pg_query($GID,$cmd);
        $columns = array();
        while($row=pg_fetch_assoc($result)) {
            $columns[] = $row['column_name'];
        }
        return $columns;
    }

}
?>
