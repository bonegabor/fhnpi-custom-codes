<?php
/* FNPI custom AJAX request functions
 *
 * */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require(getenv('OB_LIB_DIR').'db_funcs.php');
if (!$ID = PGPconnectSQL(gisdb_user,gisdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$GID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,gisdb_name,gisdb_host))
    die("Unsuccessful connect to GIS database.");

if (!$BID = PGPconnectSQL(biomapsdb_user,biomapsdb_pass,biomapsdb_name,biomapsdb_host))
    die("Unsuccesful connect to UI database.");

require(getenv('OB_LIB_DIR').'modules_class.php');
require(getenv('OB_LIB_DIR').'common_pg_funcs.php');

pg_query($ID,'SET search_path TO system,public,temporary_tables');
pg_query($GID,'SET search_path TO system,public,temporary_tables');

if(!isset($_SESSION['token'])) {
    require(getenv('OB_LIB_DIR').'prepare_vars.php');
    st_col('default_table');
}
else {
    require(getenv('OB_LIB_DIR').'prepare_session.php');
}

require(getenv('OB_LIB_DIR').'languages.php');

$MAINPAGE_PATH = getenv('PROJECT_DIR')."styles/mainpage/fhnpi_mainpage/";

require($MAINPAGE_PATH . 'mainpage_functions.php');

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

$mf = new mainpage_functions();
header("Content-type: application/json");

if (isset($_POST['pr_azonosito'])) {
    $valami = [];
    foreach ($_POST as $key => $value) {
        if ($key == 'pr_azonosito') {
            continue;
        }
        $valami[] = "$key = " . quote($value);
    }
    $cmd = sprintf("UPDATE fhnpi_metaadat SET %s WHERE pr_azonosito = %s", implode(',',$valami), quote($_POST['pr_azonosito']));
    if (! $res = pg_query($ID, $cmd)) {
        echo common_message('error','query error');
        exit;
    }
    echo common_message('ok','Projekt mentve');
    exit;
}

if (isset($_GET['reread_table'])) {    
    echo json_encode($mf->projektek() );
    exit;
}

if (isset($_GET['mp_module']) && $_GET['mp_module'] === 'last_data') {
    if (!isset($_GET['action'])) {
        echo common_message('error', 'action param missing');
        exit;
    }
    if ($_GET['action'] == 'load_data') {
        $limit = $_GET['size'] ?? 30;
        $offset = (isset($_GET['page'])) ? ($_GET['page'] - 1) * $limit : 0;
        
        echo $mf->load_data($limit, $offset);
        exit;
    }
    else {
        echo common_message('error', 'unknown action');
    }
}

?>
