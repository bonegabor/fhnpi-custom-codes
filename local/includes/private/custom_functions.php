<?php 
function custom_script_tags() {
    ob_start();
    $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    ?>
    
    <link rel="stylesheet" href="<?= $protocol ?>://<?= URL ?>/css/private/multi-select.css">
    <script type='text/javascript' src='<?= $protocol ?>://<?= URL ?>/js/private/jquery.multi-select.js'> </script>
    <script type='text/javascript' src='<?= $protocol ?>://<?= URL ?>/js/private/jquery.quicksearch.js'> </script>
    
    <?php
    echo ob_get_clean();
}
?>