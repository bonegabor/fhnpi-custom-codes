<?php

    if (!isset($_SESSION['Tid']))
        $out = login_box();
    else {

        $mf = new mainpage_functions();
        $out = "<h2>".t(str_user_statistics)."</h2>";
        $dobj = json_decode(get_profile_data(PROJECTTABLE,$_SESSION['Tcrypt']));

        $out .= "<div class='downbox'><h3>Gyűjtött rekordok száma</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$mf->gyujtott_adatok_szama());
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>Validált rekordok száma</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$mf->validalt_rekordok_szama());
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>Érvénytelen rekordok száma</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%d</li>",$mf->ervenytelen_rekordok_szama());
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>Aktivitási terület</h3>";
        $out .= "<ul class='boxul'>";
        $out .= sprintf("<li class='blarge'>%s</li>",$mf->aktivitasi_terulet() . ' km<sup>2</sup>');
        $out .= "</ul></div>";

        $out .= "<div class='downbox'><h3>".str_most_frequent_species."</h3>";
        $out .= "<ul class='boxul'>";
        foreach ($mf->top_5_faj() as $faj) {
            $out .= sprintf("<li>%s : %d</li>",$faj['mi_fajvalid'], $faj['c']);
        }
        $out .= "</ul></div>";


    }
    echo $out;

?>
