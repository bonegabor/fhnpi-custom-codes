<?php
$mf = new mainpage_functions();
?>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/styles/mainpage/fhnpi_mainpage/js/mainpage_last_data.js?rev=<?= rev($MAINPAGE_PATH . 'js/mainpage_last_data.js'); ?>"></script>
<script>
    const lastDataColumns = <?= json_encode($mf->last_data_tabulator_columns() ); ?>;
</script>
<h3><?= t('str_recent_uploads') ?></h3>
<div id="last-data"> <pre> </pre> </div>
