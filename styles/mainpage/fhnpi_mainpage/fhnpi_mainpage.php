<?php
global $OB_project_title, $MAINPAGE_PATH, $style_name, $BID;
$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';


if (file_exists(getenv('PROJECT_DIR')."local/styles/$style_name/mainpage_style.css"))
    $mainpagecss = $protocol.'://'.URL.'/local/styles/mainpage/'.$style_name.'/mainpage_style.css?rev='.rev('local/styles/mainpage/'.$style_name.'/mainpage_style.css');
else
    $mainpagecss = $protocol.'://'.URL.'/styles/mainpage/'.$MAINPAGE_TEMPLATE.'/mainpage_style.css?rev='.rev('styles/mainpage/'.$MAINPAGE_TEMPLATE.'/mainpage_style.css');

# 
# Official grid based mainpage tempalte
# Customizable
# Put your mainpage files into the private direcotry
#
#
#


$mtable = $_REQUEST['data_table'] ?? $_SESSION['current_query_table'] ?? PROJECTTABLE;
$modules = new modules($mtable);

?>
<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <meta http-equiv="cache-control" content="private, max-age=604800, must-revalidate" />

    <title>OBM - <?php echo $OB_project_title ?></title>
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $protocol ?>://<?php echo URL ?>/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $protocol ?>://<?php echo URL ?>/images/favicon-16x16.png">

    <!-- CSS icons -->    
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/fork-awesome/css/fork-awesome.min.css?rev=<?php echo rev('css/fork-awesome/css/fork-awesome.min.css'); ?>"  media="print" onload="this.media='all'; this.onload=null;" />

    <!-- pure -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/css/pure/pure-min.css?rev=<?php echo rev('css/pure/pure-min.css'); ?>" />

    <!-- jquery / ui -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.css?rev=<?php echo rev('js/ui/jquery-ui.min.css'); ?>" media="print" onload="this.media='all'; this.onload=null;" />
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.min.js?rev=<?php echo rev('js/jquery.min.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.cookie.js?rev=<?php echo rev('js/jquery.cookie.js'); ?>"></script>
    <script async defer type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/ui/jquery-ui.min.js?rev=<?php echo rev('js/ui/jquery-ui.min.js'); ?>"></script>

    <!--OpenLayers-->
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/openlayers_v6.14.1_build_ol.js"></script>
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/openlayers_v6.14.1_css_ol.css">
    <link rel="stylesheet" href="<?php echo $protocol ?>://<?php echo URL ?>/css/ol-layerswitcher.css" type="text/css" />
    <script src="<?php echo $protocol ?>://<?php echo URL ?>/js/ol-layerswitcher.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/proj4js/2.8.0/proj4.min.js"></script>
    <script>
        //let obj = { load_map_page:1 };
    </script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/jquery.awesome-cursor.min.js"></script>
    <script type="text/javascript" src="<?php echo $protocol ?>://<?php echo URL ?>/js/map_init.js?rev=<?php echo rev('js/map_init.js'); ?>"></script>

    <!-- obm -->
    <link rel="stylesheet" type="text/css" href="<?php echo $protocol ?>://<?php echo URL ?><?php echo STYLE_PATH ?>/page_style.css?rev=<?php echo rev('styles/app/'.$style_name.'/page_style.css'); ?>" />
    <script type="text/javascript"><?php include(getenv('OB_LIB_DIR').'main.js.php'); ?></script>

    <!-- mainpage style -->
    <link rel="stylesheet" type="text/css" href="<?php echo $mainpagecss ?>" />

</head>
<body onload='init();'>
<?php
//include(getenv('PROJECT_DIR').STYLE_PATH."/header.php.inc");


if (file_exists(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_grid.php"))
    require_once(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_grid.php");
else
    require_once($MAINPAGE_PATH."mainpage_grid.php");

if (file_exists(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_functions.php"))
    require_once(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_functions.php");
else 
    require_once($MAINPAGE_PATH.'mainpage_functions.php');


$mgrid = new mgrid();

?>
<script>$(document).ready(function(){});</script>
<style>
.wrapper {
    grid-gap:<?= $mgrid->gap ?>;
    grid-template-areas:<?= area($mgrid->areas) ?>;
}
@media only screen and (min-width: 600px) {
    .wrapper {
        grid-gap:<?= $mgrid->gap600 ?>;
        grid-template-columns:<?= $mgrid->columns600 ?>;
        grid-template-areas:<?= area($mgrid->areas600) ?>;
    }
}
@media only screen and (min-width: 900px) {
    .wrapper {
        grid-gap:<?= $mgrid->gap900 ?>;
        grid-template-columns:<?= $mgrid->columns900 ?>;
        grid-template-areas:<?= area($mgrid->areas900) ?>;
    }
}
@media only screen and (min-width: 1400px) {
    .wrapper {
        grid-gap:<?= $mgrid->gap1400 ?>;
        grid-template-columns:<?= $mgrid->columns1400 ?>;
        grid-template-areas:<?= area($mgrid->areas1400) ?>;
    }
}
</style>

<link rel="stylesheet" href="<?= $protocol ?>://<?= URL ?>/styles/mainpage/fhnpi_mainpage/css/tabulator.min.css?rev=<?= rev($MAINPAGE_PATH . 'css/tabulator.min.css'); ?>" type="text/css" />
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/styles/mainpage/fhnpi_mainpage/js/tabulator.min.js?rev=<?= rev($MAINPAGE_PATH . 'js/tabulator.min.js'); ?>"></script>
<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/styles/mainpage/fhnpi_mainpage/js/moment.js?rev=<?= rev($MAINPAGE_PATH . 'js/moment.js'); ?>"></script>

<?php
foreach ($mgrid->elements as $mii ) {

    if (file_exists(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_$mii.php")) {
        ob_start();
        include_once(getenv('PROJECT_DIR')."local/styles/mainpage/$style_name/mainpage_$mii.php");
        $content = ob_get_contents();
        $cellc = $content;
        ob_end_clean();
        $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
    }
    else {
        ob_start();
        include_once($MAINPAGE_PATH."mainpage_$mii.php");
        $content = ob_get_contents();
        $cellc = $content;
        ob_end_clean();
        $mgrid->definition = preg_replace("/#$mii/",$cellc,$mgrid->definition);
    }
}
echo $mgrid->definition;

/*if (!isset($mgrid->$cell))
    return;
$def = $mgrid->$cell;

$bgcol = '';
$rowspan = '';
$colspan = '';
$cellc = '';
$classes = '';

if (isset($def['bg']) and $def['bg'] == 'randomcolor')
    $bgcol = "background-color:#".rand_color();
elseif (isset($def['bg']) and $def['bg'] == 'color')
    $bgcol = "background-color:{$def['bgcolor']}";

if (isset($def['rowspan']) and $def['rowspan'])
    $rowspan = "rowspan='{$def['rowspan']}'";

if (isset($def['colspan']) and $def['colspan'])
    $rowspan = "colspan='{$def['colspan']}'";

if (isset($def['class']) and $def['class']!='')
    $classes = $def['class'];

if (file_exists("includes/$cell.php")) {
    ob_start();
    include_once("includes/$cell.php");
    $content = ob_get_contents();
    $cellc = $content;
    ob_end_clean();
    //return "<td style='$bgcol' class='$classes' $rowspan $colspan id='$cell'>$cellc</td>";
    return "<div style='$bgcol' class='box $classes' id='$cell'>$cellc</div>";
}*/

require_once(getenv('PROJECT_DIR').STYLE_PATH."/footer.php.inc");


?>
</body>
</html>
