<?php

class mainpage_functions {
    public $table;
    public $debug = FALSE;
    // data_table
    public $columns = array();
    public $headers = array();
    public $limit = 1000;
    public $orderby = 'obm_uploading_id';
    public $obm_id_order = ',obm_id DESC';
    public $order = 'DESC';
    // users' stat
    public $column = "";
    // default cache times in minutes
    public $count_members_cache = 60;
    public $count_data_cache = 10;
    public $count_uploads_cache = 10;
    public $count_species_cache = 30;
    public $stat_species_cache = 60;
    public $stat_species_user_cache = 2;
    public $most_active_users_cache = 60;
    public $force_update_cache = false;
    
    public $ervenyessegek = [];
    public $my_data = 0;

    public function __construct() {
        $this->table = (defined('DEFAULT_TABLE')) ? DEFAULT_TABLE : PROJECTTABLE;
        
        global $ID;
        
        $cmd = sprintf("SELECT DISTINCT
                            ervenyesseg, count(*) as c 
                        FROM fhnpi_search_connect sc
                        LEFT JOIN fhnpi_faj ON row_id = obm_id
                        LEFT JOIN fhnpi_search s ON sc.gyujto_ids @> ARRAY[search_id]
                        WHERE 
                            subject = 'gyujto' AND word = %s 
                        GROUP BY ervenyesseg;",
            quote($_SESSION['Tname'])
        );

        if (! $res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($ID), __FILE__, __LINE__);
            return 0;
        }
        $this->ervenyessegek = pg_fetch_all($res);

        $this->my_data = array_reduce(array_column($this->ervenyessegek,'c'),[__CLASS__,'sum']);
    }

    function returnFunc($method,$args) {
        return call_user_func(array($this, $method), $args);
    }
    
    // Summary for a custom column
    function stat_custom_column($table,$column) {
        global $ID;

        $cmd = sprintf('SELECT DISTINCT %1$s %1$s FROM %2$s WHERE %1$s IS NOT NULL ORDER BY %1$s LIMIT 10',$column,$table);
        $res = pg_query($ID,$cmd);

        return pg_num_rows($res);
    }
    // Summary sum for a custom column
    function stat_custom_column_summary($table,$column) {
        global $ID;

        $cmd = sprintf('SELECT json_agg(t) FROM (SELECT %1$s, count(%1$s) FROM %2$s WHERE %1$s IS NOT NULL GROUP BY %1$s ORDER BY count DESC LIMIT 10) t',$column,$table);
        $res = pg_query($ID,$cmd);
        $row = pg_fetch_assoc($res);

        return $row['json_agg'];
    }

    // a summary functions
    // a data table
    // default order is uploads
    function data_table($args) {

        if (!preg_match('/^'.PROJECTTABLE.'/',$this->table)) {
            return;
        }

        global $ID;
        $columns = implode(',',$this->columns);
        $cmd = sprintf("SELECT %s FROM %s ORDER BY %s %s %s LIMIT %s",$columns,$this->table,$this->orderby,$this->order,$this->obm_id_order,$this->limit);
        $res = pg_query($ID,$cmd);

        $table = new createTable();
        $table->def(['tid'=>'mytable','tclass'=>'pure pure-table pure-table-striped','tstyle'=>'width:100%']);

        // auto create link from obm_id column
        if ($this->columns[0] == 'obm_id' )
            $table->tformat(['format_col'=>'0','format_string'=>'<a href=\'data/'.$this->table.'/COL-0\' style="color:lightblue"><i class="fa fa-lg fa-newspaper-o"></i></a>']);

        while ($row=pg_fetch_assoc($res)) {
            $table->addRows($row);
        }

        $table->addHeader($this->headers);
        return $table->printOut();
    }
    // a summary functions
    // count members
    // Recommended CACHE: 6 minutes
    function count_members($project = PROJECTTABLE) {
        global $BID;

        $cnt = obm_cache('get',"count_members",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        $cmd = sprintf("SELECT count(*) as c FROM project_users WHERE project_table=%s AND user_status::varchar IN ('1','2','3','4','normal','master','operator','assistant')",quote($project));
        $result = pg_query($BID,$cmd);
        $row = pg_fetch_assoc($result);

        $time = $this->count_members_cache * 6;
        obm_cache('set',"count_members",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count data
    // Recommended CACHE: 30 minutes
    function count_data() {
        global $BID, $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$this->table)) return;

        $cnt = obm_cache('get',"count_data".$this->table,'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        // A view-kat ki kellene zárni
        $cmd = sprintf("SELECT f_project_schema,f_main_table,f_id_column FROM header_names WHERE f_table_name=%s",quote(PROJECTTABLE));
        $result = pg_query($BID,$cmd);
        $fn = 0;
        $c = 0;
        while ($row = pg_fetch_assoc($result))  {
            $cmd = sprintf("SELECT count({$row['f_id_column']}) as c FROM \"%s\".\"%s\"",$row['f_project_schema'],$row['f_main_table']);
            $result2 = pg_query($ID,$cmd);
            if ($result2) {
                $row2 = pg_fetch_assoc($result2);
                $c += $row2['c'];
            }
            $fn++;
        }

        $time = $this->count_data_cache * 30;
        obm_cache('set',"count_data".$this->table,$c,$time,FALSE);
        return $c;
    }
    // a summary functions
    // count data
    function count_tables() {
        global $ID;

        $cmd = sprintf("SELECT f_main_table FROM header_names WHERE f_table_name=%s",quote(PROJECTTABLE));
        $result = pg_query($BID,$cmd);
        return $pg_num_rows($result);

    }
    // a summary functions
    // count uplods
    // Recommended CACHE: 1 minutes
    function count_uploads($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        $cnt = obm_cache('get',"count_uploads",'','',FALSE);
        if ($this->force_update_cache) $cnt = false;
        if ($cnt !== false )
            return $cnt;

        $cmd = sprintf("SELECT count(id) as c FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        $time = $this->count_uploads_cache * 1;
        obm_cache('set',"count_uploads",$row['c'],$time,FALSE);
        return $row['c'];
    }
    // a summary functions
    // count species
    // CACHE: 2 minutes
    public function count_species($use_taxon_db=FALSE) {
        global $ID;

        $st_col = st_col($this->table,'array');
        $SPECIES_C = $st_col['SPECIES_C'];

        //if ($table_ext!='')
        //    $table = PROJECTTABLE.'_'.$table_ext;

        if ($SPECIES_C!='') {
            $cnt = obm_cache('get',"count_species",'','',FALSE);
            if ($this->force_update_cache) $cnt = false;
            if ($cnt !== false )
                return $cnt;

            $sss = preg_replace('/[a-z0-9_]+\./','',$SPECIES_C);

            #count only valid names
            if ($use_taxon_db)
                $cmd = "SELECT COUNT(DISTINCT word) AS c FROM ".PROJECTTABLE."_taxon 
                        WHERE status::varchar IN ('undefined','accepted') AND lang='".$SPECIES_C."' AND taxon_db>0";
            else
                $cmd = "SELECT COUNT($sss) AS c 
                        FROM (SELECT DISTINCT $SPECIES_C FROM ".$this->table." LEFT JOIN ".PROJECTTABLE."_taxon ON ($SPECIES_C=word) 
                        WHERE status::varchar IN ('undefined','accepted','common','synonym')) AS temp";

            $result = pg_query($ID,$cmd);
            $row=pg_fetch_assoc($result);

            $time = $this->count_species_cache * 2;
            obm_cache('set',"count_species",$row['c'],$time,FALSE);
            return  $row['c'];
        }
    }
    // a summary functions
    // count species
    // CACHE: 2 minutes
    public function stat_species($use_taxon_db=FALSE,$stat_type=array('frequent'),$addlinks=true) {

        $table = obm_cache('get',"stat_species",'','',FALSE);
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        global $ID;
        $sum = "0";
        $count = "";
        $order = "cnt";
        $where = "";

        $st_col = st_col($this->table,'array');
        $qtable = "";
        if ($this->table!='PROJECTTABLE') {
            $t = preg_replace('/'.PROJECTTABLE.'_/','',$this->table);
            $qtable = "&qtable=$t";
        }
        $SPECIES_C = $st_col['SPECIES_C'];

        if ($SPECIES_C!='') {
            $count = "count($SPECIES_C)";
        }

        if ($st_col['NUM_IND_C']!='') {
            $NUM_IND_C = $st_col['NUM_IND_C'];
            $sum = "SUM($NUM_IND_C) AS cnt";
            $where = "WHERE $NUM_IND_C IS NOT NULL AND $SPECIES_C IS NOT NULL";
        } else {
            $count .= " AS cnt";
        }

        if ($SPECIES_C!='') {

            $table = "";

            if (in_array('rarest',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$table." $where GROUP BY $SPECIES_C ORDER BY $order LIMIT 3";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small'>{$row['cnt']}</div>";
                }
                $table = "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_rarest_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }
            if (in_array('frequent',$stat_type)) {
                $list = array();
                $cmd = "SELECT $SPECIES_C as f, $count, $sum FROM ".$this->table." $where GROUP BY $SPECIES_C ORDER BY $order DESC LIMIT 6";
                $result = pg_query($ID,$cmd);
                while($row=pg_fetch_assoc($result)) {
                    if ($addlinks)
                        $link = '<a href="'.$protocol.'://'.URL.'/?query='.$SPECIES_C.':'.$row['f'].''.$qtable.'" target="_blank">'.$row['f'].'</a>';
                    else
                        $link = $row['f'];
                    $list[] = "<div class='tbl-cell button-small'>$link</div><div class='tbl-cell button-small' style='text-align:right'>".number_format($row['cnt'])."</div>";
                }
                $table .= "<div class='tbl pure-u-1'><div class='tbl-h' style='white-space:nowrap;text-decoration:underline'>".str_most_frequent_species.":</div><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";
            }

            $time = $this->stat_species_cache * 2;
            obm_cache('set',"stat_species",$table,$time,FALSE);
            return $table;
        }
    }
    // a summary functions
    // last upload date
    function last_upload($project = PROJECTTABLE) {
        global $ID;
        //if (!preg_match("/^".PROJECTTABLE."/",$project)) return;

        $cmd = sprintf("SELECT max(uploading_date) as d FROM system.uploadings WHERE project=%s",quote($project));
        $result = pg_query($ID,$cmd);
        $row=pg_fetch_assoc($result);

        return $row['d'];
    }
    // a summary functions
    // most frequent species per user
    // CACHE: 20 minutes
    function stat_species_user() {
        if (!isset($_SESSION['Tid']))
            return;

        $table = obm_cache('get',"stat_species_user");
        if ($this->force_update_cache) $table = false;
        if ($table !== false )
            return $table;

        global $ID,$BID;
        $st_col = st_col($this->table,'array');

        $table = $this->table;
        $species_c = $st_col['SPECIES_C'];

        // f_main_table????
        /*$cmd = sprintf("SELECT f_main_table,f_species_column FROM header_names WHERE f_table_name='%s' AND f_species_column IS NOT NULL",PROJECTTABLE);
        $result = pg_query($BID,$cmd);
        if (!pg_num_rows($result))
            return;
        else {
            while ($row = pg_fetch_assoc($result)) {
                $table = $row['f_main_table'];
                $species_c = $row['f_species_column'];
            }
        }*/

        $list = array();
        if ($species_c != '') {
            $cmd = sprintf('SELECT %1$s as f,count(%1$s) FROM %2$s LEFT JOIN system.uploadings ON obm_uploading_id=id 
                WHERE uploader_id=%3$d GROUP BY %1$s ORDER BY count DESC LIMIT 5',$species_c,$table,$_SESSION['Trole_id']);
            $result = pg_query($ID,$cmd);
            while($row=pg_fetch_assoc($result)) {
                $list[] = "<div class='tbl-cell'>{$row['f']}</div><div class='tbl-cell' style='text-align:right'>{$row['count']}</div>";
            }
        }
        $table = "<div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div>";

        $time = $this->stat_species_user_cache * 20;
        obm_cache('set',"stat_species_user",$table,$time);
        return $table;
    }
    
    function view_table_links ($sep='<br>') {
        global $modules;

        $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

        $links = array();

        if ($modules->is_enabled('read_table',PROJECTTABLE)) {
            $l = $modules->_include('read_table','list_links');

            foreach ($l as $link) {
                $links[] = sprintf('<a href="%1$s://%2$s/view-table/%3$s/" target="_blank">%4$s</a>', 
                                $protocol, URL, $link['link'], $link['label'] );
            }
        }
        return implode($sep,$links);

    }

    /* A <PROJECTTABLE>_search táblában kiszámolt taxon_db értéket adja vissza a felhasználó nevére.
    Egyelőre nincs kezelve az azonos nevű felhasználók problémája */

    public function gyujtott_adatok_szama() {
        return $this->my_data;
    }

    public function validalt_rekordok_szama() {
        $my_valid_data = array_values(array_filter($this->ervenyessegek,function($row) {
            return $row['ervenyesseg'] == 'ellenőrzött';
        }));
        return (count($my_valid_data)) ? $my_valid_data[0]['c'] : 0;
    }

    public function ervenytelen_rekordok_szama() {
        $ervenytelen_rekordok = array_filter($this->ervenyessegek,function($row) {
            return (in_array($row['ervenyesseg'],['hibás','valószínűleg téves']));
        });

        return array_reduce(array_column($ervenytelen_rekordok,'c'),[__CLASS__,'sum']);
    }

    public function projektben_gyujtott() {
      global $ID;  
    }
    
    public function top_5_faj() {
        global $ID;

        $cmd = sprintf("
        SELECT DISTINCT mi_fajvalid, count(*) as c FROM fhnpi_search_connect sc
        LEFT JOIN fhnpi_faj ON row_id = obm_id
        LEFT JOIN fhnpi_search s ON sc.gyujto_ids @> ARRAY[search_id]
        WHERE mi_fajvalid IS NOT NULL AND subject = 'gyujto' AND word = %s GROUP BY mi_fajvalid ORDER BY c DESC LIMIT 5;", quote($_SESSION['Tname']));

        if (! $res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($ID), __FILE__, __LINE__);
            return 0;
        }
        $result = pg_fetch_all($res);

        return $result;
    }

    private function sum($a, $b) {
        $a += $b;
        return $a;
    }

    public function aktivitasi_terulet() {
        global $ID;

        $cmd = sprintf("
        SELECT ST_Area(ST_Transform(ST_ConvexHull(ST_Collect(obm_geometry)),23700)) / 1000000 as at from
        (SELECT obm_geometry FROM fhnpi_search_connect sc
        LEFT JOIN fhnpi_faj ON row_id = obm_id
        LEFT JOIN fhnpi_search s ON sc.gyujto_ids @> ARRAY[search_id]
        WHERE subject = 'gyujto' AND word = %s) as foo;",quote($_SESSION['Tname']));
        if (! $res = pg_query($ID, $cmd)) {
            log_action(pg_last_error($ID), __FILE__, __LINE__);
            return 0;
        }
        $result = pg_fetch_assoc($res);
        $my_area = $result['at'];

        return ($this->my_data > 2) ? round($my_area,2) : 'területszámoláshoz legalább 3 feltöltött adat szükséges';
    }

    public function projektek() {
        global $ID;

        $cmd = sprintf("SELECT %s FROM public.fhnpi_metaadat;", implode(',',array_column($this->projekt_oszlopok(),'field')));
        if (!$res = pg_query($ID, $cmd)) {
            return 'Query error';
        }
        return pg_fetch_all($res);
    }

    public function projekt_oszlopok() {

        return [
            [ 'title' => 'Azonosító', 'field' => 'pr_azonosito'],
            [ 'title' => 'Projekt', 'field' => 'pr_projekt'],
            [ 'title' => 'Alprojekt', 'field' => 'pr_alprojekt'],
            [ 'title' => 'Név', 'formatter' => 'textarea', 'field' => 'pr_nev', 'width' => '250'],
            [ 'title' => 'Forrás', 'field' => 'pr_kutatas_forras'],
            [ 'title' => 'Megbízó', 'field' => 'pr_kutatas_megbizo'],
            [ 'title' => 'Kivitelező', 'field' => 'pr_kutatas_kivitelezo', 'formatter' => 'textarea', 'width' => '250'],
            [ 'title' => 'Szerződés iktatószám', 'field' => 'pr_kutatas_szerzodes_iktsz'],
            [ 'title' => 'Kezdet', 'formatter' => 'datetime', 'field' => 'pr_kutatas_kezdet','formatterParams' => ['inputFormat' => 'YYYY-MM-DD', 'outputFormat' => 'YYYY-MM-DD']],
            [ 'title' => 'Befejezés', 'formatter' => 'datetime', 'field' => 'pr_kutatas_befejezes','formatterParams' => ['inputFormat' => 'YYYY-MM-DD', 'outputFormat' => 'YYYY-MM-DD']],
            [ 'title' => 'Megjegyzés', 'formatter' => 'textarea', 'field' => 'pr_kutatas_megjegyzes', 'width' => '300'],
            [ 'title' => 'Adat lelőhelyszám', 'sorter' => 'number', 'field' => 'pr_adat_lelohelyszam'],
            [ 'title' => 'Adat rekordszám', 'sorter' => 'number', 'field' => 'pr_adat_rekordszam'],
            [ 'title' => 'Adat fotószám', 'sorter' => 'number', 'field' => 'pr_adat_fotoszam'],
            //    [ 'title' => 'Adat fotótárhely', 'sorter' => 'number', 'field' => 'pr_adat_fototarhely'],
            //    [ 'title' => 'Adat tárhely', 'sorter' => 'number', 'field' => 'pr_adat_tarhely'],
            [ 'title' => 'Citáció', 'formatter' => 'textarea', 'field' => 'pr_citacio', 'width' => '400'],
            [ 'title' => 'Kulcsszavak', 'formatter' => 'textarea', 'field' => 'pr_kulcsszavak', 'width' => '150'],
        ];
    }
    
    public function nyitott_projektek() {
        return count(array_filter($this->projektek(), function ($proj) {
            return is_null($proj['pr_kutatas_befejezes']);
        }));
    }
    
    public function lezart_projektek() {
        return count(array_filter($this->projektek(), function ($proj) {
            return !is_null($proj['pr_kutatas_befejezes']);
        }));
    }
    
    // a summary functions
    // most active users: upload count
    // CACHE: 30 minutes
    public function most_active_users($type = 'minden') {
        global $ID;

        $rows = obm_cache('get',"most_active_users_$type",'','',FALSE);
        if ($this->force_update_cache) {
            $rows = false;
        }
        if ($rows !== false ) {
            return $rows;
        }
        
        $gyujtok_where = "";
        
        $cmd = "WITH gyujtok AS (
            SELECT obm_id, mi_gyujto1 as mi_gyujto FROM fhnpi_faj %1\$s
            UNION 
            SELECT obm_id, mi_gyujto2 as mi_gyujto FROM fhnpi_faj %1\$s
        ),
        gyujto_nevek as (
            SELECT 
            CASE WHEN acc.term IS NULL THEN gy.mi_gyujto ELSE acc.term END AS uploader_name
            FROM gyujtok gy
            LEFT JOIN fhnpi_terms t ON gy.mi_gyujto = t.term AND t.subject = 'mi_gyujto'
            LEFT JOIN fhnpi_terms acc ON t.term_id = acc.term_id AND acc.subject = 'mi_gyujto' AND acc.status = 'accepted' 
            WHERE 
            gy.mi_gyujto IS NOT NULL AND 
            gy.mi_gyujto != ''
        )
        SELECT uploader_name, 0 as uploader_id, count(*) as cn FROM gyujto_nevek WHERE uploader_name != '' AND uploader_name IS NOT NULL GROUP BY uploader_name;";
        
        if ($type == 'eves') {
            $ev = date('Y');
            $gyujtok_where = " WHERE extract(year from mi_datumig) = $ev";
        }
        $cmd = sprintf($cmd, $gyujtok_where);
        $result = pg_query($ID,$cmd);

        $counter = array();
        while ($row = pg_fetch_assoc($result)) {
            if (!isset($counter[$row['uploader_name']]))
                $counter[$row['uploader_name']] = 0;
            $counter[$row['uploader_name']] += $row['cn'];

        }
        arsort($counter);
        $rows = array();
        foreach($counter as $name=>$cn) {
            $rows[] = array('uploader_name'=>$name,'uploader_id'=>0,'cn'=>$cn);
        }
        $rows = array_slice($rows, 0, 10);

        $time = $this->most_active_users_cache* 60;
        obm_cache('set', "most_active_users_$type", $rows, $time, FALSE);
        return $rows;
    }
    
    public function last_data_tabulator_columns () {
        $stable_params = $this->last_data_columns();
        return array_map(function ($name, $col) {
            $colparams = [ 
                'title' => $name, 
                'field' => $col,
                'formatter' => (in_array($col, ['obm_id', 'obm_files_id'])) ? 'html' : 'plaintext'
            ];
            if ($col === 'obm_id') {
                $colparams['width'] = 60;
            }
            return $colparams;
        }, $stable_params, array_keys($stable_params));
        
    }
    
    private function last_data_columns() {
        global $modules, $x_modules;
        
        $columns = obm_cache('get',__FUNCTION__,'','',FALSE);
        if ($columns !== false ) {
            return $columns;
        }
        
        $dbcolist = dbcolist('array', DEFAULT_TABLE);
        $stable_params = ['obm_id' => 'obm_id'];
        foreach ($modules->_include('results_asStable', 'get_params') as $v) {
            if ($v == 'obm_uploader_name' || $v == 'obm_uploader_user') {
                $v = 'uploader_name';
            }
            if ($v == 'obm_filename' ) {
                $v = 'filename';
            }
            if ($v === 'obm_taxon') {
                $st_col = st_col("", "array");
                $v = $st_col['SPECIES_C'];
            }
            if (isset($dbcolist[$v])) {
                $stable_params[$v] = $dbcolist[$v];
            }
        }
        obm_cache('set', __FUNCTION__, $stable_params, 300, FALSE);
        return $stable_params;
    }
    
    public function load_data($limit, $offset) {
        global $ID, $modules, $x_modules;
        
        $data = obm_cache('get',"load_data_{$limit}_{$offset}",'','',FALSE);
        
        if ($data !== false ) {
            return json_encode($data);
        }
        $data = [];
        
        $_REQUEST = ["qids_obm_id" => "last_data_{$limit}_{$offset}"];
        $_SESSION['show_results']['type'] = 'json';
        $_SESSION['current_query_table'] = 'fhnpi_faj';

        include(getenv('OB_LIB_DIR').'query_builder.php');
        include(getenv('OB_LIB_DIR').'results_query.php');
        include_once(getenv('OB_LIB_DIR').'results_builder.php');

        $r = new results_builder(['method'=>'json','specieslist'=>0,'buttons'=>0,'summary'=>0,'filename'=>""]);
        
        if( $r->results_query() ) {
            if (!$r->rq['results_count']) {
                return common_message('failed','Results count: 0');
            } 
            else {
                $load_data_columns = array_map(function ($col) {
                    if ($col === 'obm_id') {
                        $col = "'<a href=\"data/fhnpi_faj/' || obm_id || '/\" target=\"_blank\">' || obm_id || '</a>' as obm_id";
                    }
                    elseif ($col === 'obm_files_id') {
                        $col = "CASE WHEN obm_files_id IS NULL THEN NULL ELSE '<a href=\"getphoto?connection=' || obm_files_id || '\" class=\"photolink\"><i class=\"fa fa-camera\"></i></a>' END AS obm_files_id";
                    }
                    elseif ($col === 'mi_datumig') {
                        $col = 'mi_datumig::date AS mi_datumig';
                    }
                    return $col;
                }, array_keys($this->last_data_columns()));
                
                $cmd = sprintf('SELECT %3$s FROM temporary_tables.temp_query_%1$s_%2$s ORDER BY mi_datumig DESC',PROJECTTABLE,session_id(), implode(',', $load_data_columns));
                if (!$res = pg_query($ID, $cmd)) {
                    return common_message('error', 'query_failed');
                }
            }
        }
        
        unset($_SESSION['show_results']);
        
        $data = [
            "last_page" => 1000,
            "data" => pg_fetch_all($res)
        ];
        
        obm_cache('set', "load_data_{$limit}_{$offset}", $data, 300, FALSE);
        return json_encode($data);
    }
    
    private function delete_col(&$array, $key) {
        // Check that the column ($key) to be deleted exists in all rows before attempting delete
        foreach ($array as &$row)   { if (!array_key_exists($key, $row)) { return false; } }
        foreach ($array as &$row)   { unset($row[$key]); }

        unset($row);

        return true;
    }
}
?>
