<?php
$mf = new mainpage_functions();

$protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';

?>

<script>
    const columnData = <?= json_encode($mf->projekt_oszlopok() ); ?>;
    let tableData = <?= json_encode($mf->projektek() ); ?>;
</script>

<script type="text/javascript" src="<?= $protocol ?>://<?= URL ?>/styles/mainpage/fhnpi_mainpage/js/projekt-nyilvantarto.js?rev=<?= rev($MAINPAGE_PATH . 'js/projekt-nyilvantarto.js'); ?>"></script>

<form id="projekt_szerkeszto" class="pure-form pure-form-stacked">
    <fieldset>
        <legend></legend>

        <div class="pure-g">
        <?php foreach ($mf->projekt_oszlopok() as $o): ?>
            <?php $readonly = ($o['field'] === 'pr_azonosito') ? 'readonly' : '';
            $type = 'text';
            if (in_array($o['field'],['pr_azonosito','pr_projekt','pr_alprojekt','pr_adat_lelohelyszam','pr_adat_rekordszam','pr_adat_fotoszam'])) {
                $col_size = 3;

            }
            elseif (in_array($o['field'],['pr_kutatas_kezdet','pr_kutatas_befejezes'])) {
                $col_size = 2;
                $type = 'date';
            }
            else {
                $col_size = 1;
            } ?>
            <div class="pure-u-1 pure-u-md-1-<?= $col_size ?>">
                <label for="<?= $o['field'] ?>"><?= $o['title'] ?><label>
                <input id="<?= $o['field'] ?>" name="<?= $o['field'] ?>" class="pure-u-1-1" type="<?= $type ?>" <?= $readonly; ?>>
            </div>
        <?php endforeach; ?>
        </div>
    </fieldset>
</form>

<h3> Projekt nyilvántartó </h3>
    <div class="table-controls">
    </div>
    <div id="projekt_nyilvantarto-gombok">
        <button id="add-filter" class="pure-button button-href"> <i class="fa fa-filter" aria-hidden="true"></i> Új szűrő</button>
        <button id="filter-clear" class="pure-button button-href"> <i class="fa fa-trash-o" aria-hidden="true"></i> Szűrők törlése</button>

<?php if (in_array(ADATMANAGER_GROUP_ID, explode(',',$_SESSION['Tgroups']))) : ?>
        <a href="<?= $protocol ?>://<?= URL ?>/upload/?form=<?= PROJEKT_URLAP_ID ?>&type=web" id="new-project" class="pure-button button-href button-success"> <i class="fa fa-plus" aria-hidden="true"></i> Új projekt</a>
<?php endif; ?>
    </div>
<div id="projekt-nyilvantarto"> </div>
