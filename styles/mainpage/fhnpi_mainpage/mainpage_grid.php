<?php
if (has_access('master')) {
    class mgrid {
        // `elements`.php should be exists in local includes dir:
        // e.g. includes/sidebar1.php ...
        var $elements = array('header','sidebar1','sidebar2','projekt_nyilvantarto','footer', 'content1', 'last_data');
        
        // default layout - will be overdefined width grids
        var $definition = '
        <div class="wrapper">
          <div class="box header">#header</div>
          <div class="box sidebar1">#sidebar1</div>
          <div class="box sidebar2">#sidebar2</div>
          <div class="box content1">#content1</div>
          <div class="box last_data">#last_data</div>
          <div class="box projekt_nyilvantarto">#projekt_nyilvantarto</div>
          <div class="box footer">#footer</div>
        </div>';
     
       // minimal width layout
       var $areas = '
         header     
         sidebar1
         sidebar2
         content1
         last_data
         projekt_nyilvantarto
         footer';
       var $gap = "1em";

       // min 600px width layout
       var $gap600 = '2%';
       var $columns600 = '49% 49%';
       var $areas600 = 'header header
                        sidebar2 sidebar1
                        content1 content1
                        last_data last_data
                        projekt_nyilvantarto projekt_nyilvantarto
                        footer   footer'; 

       // min 900px width layout
       var $gap900 = '1%';
       var $columns900 = '33% 32% 33%';
       var $areas900 = 'header header header
                        sidebar2 sidebar1 content1
                        sidebar2 sidebar1 footer
                        last_data last_data last_data
                        projekt_nyilvantarto projekt_nyilvantarto projekt_nyilvantarto'; 

       // min 1400 px width layout
       var $gap1400 = '1%';
       var $columns1400 = '33% 32% 33%';
       var $areas1400 = 'header header header
                         sidebar2 sidebar1 content1
                         sidebar2 sidebar1 footer
                         last_data last_data last_data
                         projekt_nyilvantarto projekt_nyilvantarto projekt_nyilvantarto'; 
    }
}
elseif (has_access('projekt_nyilvantarto')) {
    class mgrid {
        // `elements`.php should be exists in local includes dir:
        // e.g. includes/sidebar1.php ...
        var $elements = array('header','sidebar1','sidebar2','projekt_nyilvantarto','footer', 'content1');
        
        // default layout - will be overdefined width grids
        var $definition = '
        <div class="wrapper">
          <div class="box header">#header</div>
          <div class="box sidebar1">#sidebar1</div>
          <div class="box sidebar2">#sidebar2</div>
          <div class="box content1">#content1</div>
          <div class="box projekt_nyilvantarto">#projekt_nyilvantarto</div>
          <div class="box footer">#footer</div>
        </div>';
     
       // minimal width layout
        var $areas = '
         header
         sidebar1
         sidebar2
         content1
         projekt_nyilvantarto
         footer';
       var $gap = "1em";

       // min 600px width layout
       var $gap600 = '2%';
       var $columns600 = '49% 49%';
       var $areas600 = 'header header
                        sidebar2 sidebar1
                        content1 content1
                        projekt_nyilvantarto projekt_nyilvantarto
                        footer   footer'; 

       // min 900px width layout
       var $gap900 = '1%';
       var $columns900 = '33% 32% 33%';
       var $areas900 = 'header header header
                        sidebar2 sidebar1 content1
                        sidebar2 sidebar1 footer
                        projekt_nyilvantarto projekt_nyilvantarto projekt_nyilvantarto'; 

       // min 1400 px width layout
       var $gap1400 = '1%';
       var $columns1400 = '33% 32% 33%';
       var $areas1400 = 'header header header
                         sidebar2 sidebar1 content1
                         sidebar2 sidebar1 footer
                         projekt_nyilvantarto projekt_nyilvantarto projekt_nyilvantarto'; 
    }
}
else {
    class mgrid {
        // `elements`.php should be exists in local includes dir:
        // e.g. includes/sidebar1.php ...
        var $elements = array('header','sidebar1','sidebar2','content1','content2','footer');
        
        // default layout - will be overdefined width grids
        var $definition = '
        <div class="wrapper">
          <div class="box header">#header</div>
          <div class="box sidebar1">#sidebar1</div>
          <div class="box sidebar2">#sidebar2</div>
          <div class="box content1">#content1</div>
          <div class="box footer">#footer</div>
        </div>';
     
       // minimal width layout
        var $areas = 'header
         content1
         sidebar1
         sidebar2
         footer';
       var $gap = "1em";

       // min 600px width layout
       var $gap600 = '5px';
       var $columns600 = '50% auto';
       var $areas600 = 'header header
                        content1 content1
                        sidebar1 sidebar2
                        sidebar1 footer'; 

       // min 900px width layout
       var $gap900 = '10px';
       var $columns900 = '34% 33% auto';
       var $areas900 = 'header header header
                        content1 content1 content1 
                        sidebar1 sidebar1 sidebar2
                        sidebar1 sidebar1 footer';

       // min 1400 px width layout
       var $gap1400 = '20px';
       var $columns1400 = 'auto 550px';
       var $areas1400 = 'header header
                         content1 sidebar1
                         content1 sidebar1
                         sidebar2 sidebar1
                         sidebar2 footer'; 
    }
}
?>

