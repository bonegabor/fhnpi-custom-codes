$(document).ready(function() {
    var table = new Tabulator("#last-data", {
        height: "311px",
        layout: "fitColumns",
        groupBy: "mi_datumig",
        ajaxURL: 'fhnpi_ajax',
        ajaxParams: {
            mp_module: "last_data",
            action: "load_data"
        },
        ajaxProgressiveLoad: "scroll",
        ajaxProgressiveLoadScrollMargin: 1000,
        paginationSize: 100,
        placeholder: "No Data Set",
        columns: lastDataColumns,
    });
})

