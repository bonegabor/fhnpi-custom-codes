    let t_row = {};
$(document).ready(function() {
    //Custom filter example
    let pFilter = {
        field: '',
        type: '',
        value: ''
    };
    function addFilter() {
        //determine the number of filters
        let n = $('.filter').length;
        n++;

        //prepare the options for the field selector
        let fieldOptions = '';
        $.each(columnData, function(k,el) {
            fieldOptions += '<option value="' + el.field + '">' + el.title + '</option>';
        });

        //relation options
        const typeOptions = '<option value="like">like</option> <option value="=">=</option> <option value="<">&lt;</option> <option value="<=">&lt;=</option> <option value=">">&gt;</option> <option value=">=">&gt;=</option> <option value="!=">!=</option>';

        //build the html
        let html = '<div class="filter">';
        html += '<span> <label>Mező: </label> <select id="filter-field-' + n + '" class="filter-field" data-n="' + n + '"> <option> </option> ' + fieldOptions + '</select> </span>';
        html += '<span> <label>Kapcsolat: </label> <select id="filter-type-' + n + '" class="filter-type" data-n="' + n + '"> ' + typeOptions + ' </select> </span>';
        html += '<span><label>Érték: </label> <input id="filter-value-' + n + '" type="text" placeholder="value to filter" class="filter-value" data-n="' + n + '"></span>';


        $('.table-controls').append(html);

        //Update filters on value change
        $("#filter-field-" + n + ", #filter-type-" + n).change(function() {
            updateFilter(n);
        });
        $("#filter-value-" + n).keyup(function() {
            updateFilter(n);
        });


    }
    //Add more filter
    $("#add-filter").click(function() {
        addFilter();
    });

    //Clear filters on "Clear Filters" button click
    $("#filter-clear").click(function(){
        $(".filter").remove();
        addFilter();

        table.clearFilter();
    });

    //Trigger setFilter function with correct parameters
    function updateFilter(n){
        filter = {
            field: $("#filter-field-" + n).val(),
            type: $("#filter-type-" + n).val(),
            value: $("#filter-value-" + n).val(),
        }

        let filters = table.getFilters();
        filters[n] = filter;
        table.setFilter(filters);
    }

    let projekt_mentes = function() {
        const data = new FormData(document.getElementById('projekt_szerkeszto'));
        var request = new XMLHttpRequest();
        request.open("POST", "fhnpi_ajax", true);
        request.onreadystatechange = function () {
            if(request.readyState === XMLHttpRequest.DONE && request.status === 200) {
                var request2 = new XMLHttpRequest();
                request2.open("GET","fhnpi_ajax?reread_table")
                request2.onreadystatechange = function () {
                    console.log(request2.readyState);
                    console.log(request2.status);
                    if(request2.readyState === XMLHttpRequest.DONE && request2.status === 200) {
                        tableData = request2.responseText;
                        console.log(tableData);
                        table.setData(tableData);
                    }
                }
                request2.send(null)
            }
        }
        request.send(data);
        $( this ).dialog( "close" );
    }

    $( "#projekt_szerkeszto" ).dialog({
        autoOpen: false,
        title: "Projekt szerkesztés",
        modal: true,
        width: 700,
        height: 500,
        draggable: true,
        resizable: true,
        buttons: [
            {
                text: "Mentés",
                icon: "ui-icon-heart",
                click: projekt_mentes,
                id: "project_mentes"
            },
            {
                text: "Mégsem",
                icon: "ui-icon-heart",
                click: function() {
                    $( this ).dialog( "close" );
                }
            }
        ]
    });

    columnData.unshift({
        title: 'Szerkesztés',
        field: 'edit_pr',
        formatter: 'html',
        align: 'center',
        cellClick:function(e, cell){
            const data = cell.getData();
            $.each(data, function (key, value) {
                if (key != 'edit_pr' && key != 'pr_azonosito_link') {
                    $('#'+key).val(value);
                }
            })
            $( "#projekt_szerkeszto" ).dialog( "open" );
            
            //modal.style.display = "block";
        },
    });
    columnData.unshift({
        title: 'Adatok',
        field: 'pr_azonosito_link',
        formatter: 'html',
        align: 'center',
    });
    
    tableData.map(r => {
        r.pr_azonosito_link = "<a href='https://fhnpi.openbiomaps.org/projects/fhnpi/index.php?query_api={\"qids_pr_alprojekt\":\"" + r.pr_azonosito + "\"}' target='_blank'> <i class='fa fa-globe'> </i> </a>";
        r.edit_pr = "<i class='fa fa-pencil-square-o'> </i>";
    });
        
    //'' as pr_azonosito_link
    var table = new Tabulator("#projekt-nyilvantarto", {
        columns: columnData,
        index: "pr_azonosito",
        groupBy: "pr_projekt",
        height:"430px",
        pagination:"local",
        paginationSize:12,
        paginationSizeSelector:[12, 24, 50],
    });
    table.setData(tableData);

    addFilter();
});
