<?php
    $mf = new mainpage_functions();
?>
<h2><?php echo t('str_database_statistics')?></h2>
<div class="box-container">
    <div class='boox boxborder'><h3><?php echo t('str_members')?></h3>
        <ul class='boxul'>
            <li class='blarge'><?php echo $mf->count_members(); ?></li>
        </ul>
    </div>
    
    <div class='boox boxborder'><h3><?php echo t('str_uploads')?></h3>
        <ul class='boxul'>
            <li class='blarge'><?php $mf->count_uploads_cache = 5;echo $mf->count_uploads(); ?></li>
        </ul>
    </div>
    
    <div class='boox boxborder'><h3><?php echo t('str_data')?></h3>
        <ul class='boxul'>
            <li class='blarge'>
                <?php 
                $mf->table = defined('DEFAULT_TABLE') ? DEFAULT_TABLE : PROJECTTABLE ;
                echo $mf->count_data(); 
                ?>
            </li>
        </ul>
    </div>
    
    <div class='boox boxborder'><h3><?php echo t('str_species')?></h3>
        <ul class='boxul'>
            <li class='blarge'><a href='?specieslist'><?php $mf->count_species_cache = 5; echo $mf->count_species(true)?></a></li>
        </ul>
    </div>
    
    <div class='boox boxborder'><h3><?php echo t('str_species_stat')?></h3>
        <ul class='boxul'>
            <li><?php echo $mf->stat_species()?></li>
        </ul>
    </div>
    
    <div class='boox boxborder'><h3><?php echo t('str_projects')?></h3>
        <ul class='boxul'>
            <li><?php echo t('str_nyitott_projektek') . ': ' . $mf->nyitott_projektek()?></li>
            <li><?php echo t('str_lezart_projektek') . ': ' . $mf->lezart_projektek()?></li>
        </ul>
    </div>
    
    <div class='boox boxborder most-data'><h3><?php echo t('str_most_data')?></h3>
        <ul class='boxul'>
            <li>
                <div class='tbl pure-u-1'>
                    <?= implode("", array_map(function ($row) {
                        return "<div class='tbl-row'><div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div></div>";
                    }, $mf->most_active_users('minden'))) ?>
                </div>
            </li>
        </ul>
    </div>
    
    <div class='boox boxborder most-data'><h3><?php echo t('str_most_data') . ' - ' . date('Y')?></h3>
        <ul class='boxul'>
            <li>
                <div class='tbl pure-u-1'>
                    <?= implode("", array_map(function ($row) {
                        return "<div class='tbl-row'><div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div></div>";
                    }, $mf->most_active_users('eves'))) ?>
                </div>
            </li>
        </ul>
    </div>
</div>
