<?php
    $ref =  sprintf('%s://%s/',$protocol,URL);
    $out = "<div class='centerbox'><a href='".$ref."upload/'  class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-binoculars'></i> ".str_observation_upload."</a></div>";
    $out .= "<div class='centerbox'><a href='".$ref."index.php?map' class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-search'></i> ".str_data_query."</a></div>";
    if (isset($_SESSION['Tcrypt'])) {
        $profile = "profile/" . $_SESSION['Tcrypt'];
        $out .= "<div class='centerbox'><a href='".$ref.$profile."' class='pure-button button-xlarge button-href pure-u-1'><i class='fa fa-user'></i> ".str_profile."</a></div>";
    }

    echo $out;
?>
