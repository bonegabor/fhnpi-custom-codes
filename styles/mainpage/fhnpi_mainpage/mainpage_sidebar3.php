<?php

    $mf = new mainpage_functions();

    $out = "<h2>".t(str_activity)."</h2>";

    $out .= "<div class='downbox'><h3>".str_most_data."</h3>";
    $out .= "<ul class='boxul'>";
    #$mf->column = "observer";
    #$rows = $mf->most_active_users('custom');
    $rows = $mf->most_active_users('data');
    $list = array();
    foreach($rows as $row) {
        $list[] = "<div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div>";
    }
    $out .= "<li><div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div></li>";

    $out .= "</ul></div>";

    $out .= "<div class='downbox'><h3>".str_most_uploads."</h3>";
    $out .= "<ul class='boxul'>";
    $rows = $mf->most_active_users('uploads');

    $list = array();
    foreach($rows as $row) {
        $list[] = "<div class='tbl-cell'>{$row['uploader_name']}</div><div class='tbl-cell' style='text-align:right'>{$row['cn']}</div>";
    }
    $out .= "<li><div class='tbl pure-u-1'><div class='tbl-row'>".implode("</div><div class='tbl-row'>",$list)."</div></div></li>";

    $out .= "</ul></div>";

    echo $out;
?>
