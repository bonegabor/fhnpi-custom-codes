# path of table list
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
table_list="$DIR/.archive_list.txt"

# postgres parameters
project_database="gisdata"
system_database="biomaps"
admin_user="biomapsadmin"
archive_path="/home/archives"
pgport="5432"
docker="/usr/bin/docker compose -f /srv/docker/openbiomaps/docker-compose.yml exec -T"
pg_dump_gisdata="$docker gisdata11 pg_dump -p $pgport"
pg_dump_biomaps="$docker biomaps11 pg_dump -p $pgport"
psql_gisdata="$docker gisdata11 psql -p $pgport"
psql_biomaps="$docker biomaps11 psql -p $pgport"

