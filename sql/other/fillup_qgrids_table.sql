DO $$
    DECLARE 
        rec RECORD;
	etrs10 geometry;
	etrs1 geometry;
	utm10 geometry;
	utm5 geometry;
	geom geometry;
	centr geometry;
    BEGIN
        FOR rec IN (SELECT * FROM fhnpi_faj ORDER BY obm_id) LOOP

	    geom := rec."obm_geometry";
	    centr := ST_Centroid(geom);

            SELECT geometry INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);
            SELECT geometry INTO utm10 FROM shared.utm10 g WHERE ST_Intersects(centr, g.geometry);
            SELECT geometry INTO etrs1 FROM shared.etrs1 g WHERE ST_Intersects(centr, g.geometry);
            SELECT geometry INTO utm5 FROM shared.utm5 g WHERE ST_Intersects(centr, g.geometry);

            EXECUTE format('INSERT INTO fhnpi_faj_qgrids ("row_id", "original", "etrs10", "utm10", "centroid","etrs1","utm5") VALUES (%L,%L,%L,%L,%L,%L,%L);', rec.obm_id, geom, etrs10, utm10, centr, etrs1, utm5 );

        END LOOP;

    END$$
    ;


