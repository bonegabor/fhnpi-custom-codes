INSERT INTO fhnpi_rules ("data_table","row_id","write","read","sensitivity")
SELECT 'fhnpi_faj',obm_id, '{30}', '{30}', 3
FROM fhnpi_faj WHERE obm_uploading_id IS NULL;

INSERT INTO fhnpi_rules ("data_table","row_id","write","read","sensitivity")
SELECT 'fhnpi_faj',obm_id,ARRAY[uploader_id],ARRAY[uploader_id],2 
FROM uploadings u 
LEFT JOIN fhnpi_faj f ON u.id = f.obm_uploading_id
