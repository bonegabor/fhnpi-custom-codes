SELECT count(*) FROM fhnpi_rules WHERE NOT "read" @> ARRAY[26];

BEGIN;
UPDATE fhnpi_rules fr
SET 
    "read" = "read" || 26
WHERE 
    NOT "read" @> ARRAY[26] ;
COMMIT;


-- erre még rá kell kérdezni
SELECT count(*) FROM fhnpi_rules WHERE NOT "write" @> ARRAY[26]
BEGIN;
UPDATE fhnpi_rules fr
SET 
    "write" = "write" || 26
WHERE 
    NOT "write" @> ARRAY[26] ;
ROLLBACK;
    --"write" = "write" || 26
