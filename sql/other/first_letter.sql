ALTER TABLE shared."fhnpi_nyilvtar_hrszalreszlet" ADD COLUMN obm_first_letter_filter character(1);
UPDATE shared.fhnpi_nyilvtar_hrszalreszlet SET obm_first_letter_filter=lower(substring(dat_telepu from 1 for 1));
EXPLAIN ANALYZE SELECT DISTINCT "dat_telepu" "dat_telepu" FROM shared.fhnpi_nyilvtar_hrszalreszlet WHERE obm_first_letter_filter='a' AND dat_telepu ILIKE 'abd%' ORDER BY dat_telepu LIMIT 20;

CREATE UNIQUE INDEX fhnpi_nyilvtar_hrszalreszlet_pkey ON fhnpi_nyilvtar_hrszalreszlet USING btree (ogc_fid);
CREATE INDEX idx_fhnpi_nyilvtar_hrszalreszlet_geometry ON fhnpi_nyilvtar_hrszalreszlet USING gist (geometry);
CREATE INDEX telepules_idx ON fhnpi_nyilvtar_hrszalreszlet USING btree (dat_telepu, obm_first_letter_filter);
