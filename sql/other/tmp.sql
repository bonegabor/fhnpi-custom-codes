explain (
    ANALYZE,
    COSTS,
    VERBOSE,
    BUFFERS,
    format json
)
SELECT f.obm_id,
    qgrids."original" AS obm_geometry,
    1 AS selected
FROM fhnpi_taxon
    INNER JOIN fhnpi_faj f ON mi_fajmagyar = word 
    INNER JOIN fhnpi_vedettseg v ON f.mi_fajvalid = v.faj
    INNER JOIN fhnpi_rules data_rules ON (
        data_rules.data_table = 'fhnpi_faj'
        AND f.obm_id = data_rules.row_id
    )
    
    INNER JOIN fhnpi_taxonmeta txm ON fhnpi_taxon.taxon_id = txm.taxon_id
    INNER JOIN fhnpi_faj_qgrids qgrids ON (f.obm_id = qgrids.row_id)
    INNER JOIN system.fhnpi_faj_linnaeus linn ON (linn.row_id = f.obm_id)
WHERE 
    fhnpi_taxon.taxon_id IN (8815) AND lang = 'mi_fajmagyar';