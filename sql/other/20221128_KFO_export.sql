COPY (
    WITH d as (
SELECT 
    obm_id,
    file_id, 
    coalesce(LOWER(REPLACE(REPLACE(mi_fajlatin, ' ', '_'), '.', '')), 'NA') as faj,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM fhnpi_faj
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    pr_alprojekt = 'fh_kozos_kfo22' AND
    obm_files_id IS NOT NULL 
)
SELECT 'cp ./attached_files/' || reference || ' ./downloads/' || obm_id || '_' || file_id || '_E' || x || '_N' || y || '_' || faj || '.jpg' FROM d
)
TO '/tmp/fh_kozos_kfo22_kepek.sh' CSV;


SELECT count(*) FROM fhnpi_faj WHERE pr_alprojekt = 'fh_kozos_kfo22';
SELECT count(*) FROM fhnpi_faj WHERE pr_alprojekt = 'fh_kozos_kfo22' AND obm_files_id IS NOT NULL;

-- 2023
COPY (
    WITH d as (
SELECT 
    obm_id,
    file_id, 
    coalesce(LOWER(REPLACE(REPLACE(mi_fajlatin, ' ', '_'), '.', '')), 'NA') as faj,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM fhnpi_faj
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    pr_alprojekt = 'fh_kozos_kfo23' AND
    obm_files_id IS NOT NULL 
)
SELECT 'cp ./attached_files/' || reference || ' ./downloads/' || obm_id || '_' || file_id || '_E' || x || '_N' || y || '_' || faj || '.jpg' FROM d
)
TO '/tmp/fh_kozos_kfo23_kepek.sh' CSV;


SELECT count(*) FROM fhnpi_faj WHERE pr_alprojekt = 'fh_kozos_kfo23';
SELECT count(*) FROM fhnpi_faj WHERE pr_alprojekt = 'fh_kozos_kfo23' AND obm_files_id IS NOT NULL;
