-- itt át is méreteztem a képeket, hogy 1500x1500 keretbe beférjenek
--convert attached_files/3__1_.jpg -resize 1500x1500\> downloads/3__1_resizd.jpg
COPY (
    WITH d as (
SELECT 
    obm_id,
    file_id, 
    coalesce(LOWER(REPLACE(REPLACE(mi_fajlatin, ' ', '_'), '.', '')), 'NA') as faj,
    SUBSTRING(REPLACE(ST_X(obm_geometry)::text, '.', '-') from 1 for 8) as x,
    SUBSTRING(REPLACE(ST_y(obm_geometry)::text, '.', '-') from 1 for 8) as y,
    reference
FROM fhnpi_faj
LEFT JOIN system.file_connect fc ON obm_files_id = conid
LEFT JOIN system.files f ON f.id = fc.file_id
where 
    mi_elall = 'Tetem – áramütött' AND
    obm_files_id IS NOT NULL 
)
SELECT 'convert ./attached_files/' || reference || ' -resize 1500x1500\> ./downloads/' || obm_id || '_' || file_id || '_E' || x || '_N' || y || '_' || faj || '.jpg' FROM d
)
TO '/tmp/elall_aramutott_kepek.sh' CSV;

