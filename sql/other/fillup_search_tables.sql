DO $$
    DECLARE 
        observer RECORD;
        rec RECORD;
        gyid integer;
        gyidk integer[] DEFAULT '{}';
        akid integer;
        hatid integer;
    BEGIN
        FOR rec IN (SELECT * FROM fhnpi_faj ORDER BY obm_id) LOOP

            gyidk := '{}';
            FOR observer IN (SELECT trim( unnest( regexp_split_to_array( concat_ws(',', rec.mi_gyujto1, rec.mi_gyujto2, rec.mi_gyujto3),E'[,;]\\s*') ) ) as o) LOOP 
                SELECT add_term(observer.o, 'fhnpi_faj', 'gyujto', 'fhnpi') INTO gyid;
                gyidk := gyidk || gyid;
            END LOOP;

            SELECT add_term(rec.mi_adatkozlo, 'fhnpi_faj', 'adatkozlo', 'fhnpi') INTO akid;
            SELECT add_term(rec.mi_hatarozo, 'fhnpi_faj', 'hatarozo', 'fhnpi') INTO hatid;


            EXECUTE format('INSERT INTO fhnpi_search_connect (data_table, row_id, gyujto_ids, adatkozlo_ids, hatarozo_ids) VALUES (%1$L, %2$L, %3$L, ''{%4$s}'', ''{%5$s}'');', 'fhnpi_faj', rec.obm_id, gyidk, akid, hatid );

        END LOOP;

    END$$
    ;
