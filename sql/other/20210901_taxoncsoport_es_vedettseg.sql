WITH dt as (
    SELECT DISTINCT taxon_id, word, vedettseg, taxon
    FROM fhnpi_vedettseg
    LEFT JOIN fhnpi_taxon ON faj = word
    )
UPDATE fhnpi_taxonmeta tm SET taxon_csoport = taxon, vedettsegi_status = dt.vedettseg FROM dt WHERE tm.taxon_id = dt.taxon_id;
