
WITH rows AS (
    SELECT DISTINCT f.mi_fajered,
        foo.valid_word
    FROM fhnpi_faj f
        LEFT JOIN (
            SELECT t.word as src_word,
                CASE
                    WHEN tt.word IS NULL THEN fb.word
                    ELSE tt.word
                END as valid_word
            FROM fhnpi_taxon t
                LEFT JOIN fhnpi_taxon tt ON t.taxon_id = tt.taxon_id
                AND tt.status = 'accepted'
                LEFT JOIN fhnpi_taxon fb ON t.taxon_id = fb.taxon_id
                AND fb.lang = 'mi_fajlatin'
                AND fb.status = 'accepted'
            WHERE tt.lang = 'mi_fajlatin'
                AND tt.word IS NOT NULL
        ) foo ON f.mi_fajered = foo.src_word
    WHERE f.mi_fajlatin IS DISTINCT
    FROM foo.valid_word
)
SELECT distinct rows.mi_fajered FROM
fhnpi_faj ff
INNER JOIN rows
ON rows.mi_fajered = ff.mi_fajered
    AND ff.mi_fajlatin IS DISTINCT
FROM rows.valid_word;