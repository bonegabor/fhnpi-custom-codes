CREATE OR REPLACE FUNCTION "public"."update_fhnpi_faj_taxonlist" () RETURNS trigger AS 
$$
-- DECLARE
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO fhnpi_taxon (word,lang,modifier_id) 
            SELECT new.mi_fajered,'mi_fajered',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM fhnpi_taxon WHERE new.mi_fajered = word
            ) AND new.mi_fajered IS NOT NULL; 
        
        RETURN new;
    END IF;
    IF tg_op = 'UPDATE' THEN
        INSERT INTO fhnpi_taxon (word,lang,modifier_id) 
            SELECT new.mi_fajered,'mi_fajered',new.obm_modifier_id 
            WHERE NOT EXISTS ( 
                    SELECT taxon_id FROM fhnpi_taxon WHERE new.mi_fajered = word
            ) AND new.mi_fajered IS NOT NULL; 
        RETURN new;
    END IF;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
