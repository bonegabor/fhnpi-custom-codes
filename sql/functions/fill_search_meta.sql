CREATE OR REPLACE FUNCTION "fill_search_meta" () RETURNS trigger LANGUAGE plpgsql AS $$
BEGIN
    IF (TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND NEW.word != OLD.word)) THEN
        NEW.meta = lower(unaccent(NEW.word));
    END IF;
    
    RETURN NEW;
END;
$$;
