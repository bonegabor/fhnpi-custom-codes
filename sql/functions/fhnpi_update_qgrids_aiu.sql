CREATE OR REPLACE FUNCTION "public"."fhnpi_faj_update_qgrids" () RETURNS trigger AS 
$$
DECLARE
 tbl varchar;
 geom geometry;
 centr geometry;
 etrs10 geometry;
 utm10 geometry;
BEGIN
        IF tg_op = 'INSERT' THEN

            geom := new."obm_geometry";
	    centr := ST_Centroid(geom);

            SELECT geometry INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);
            SELECT geometry INTO utm10 FROM shared.utm10 g WHERE ST_Intersects(centr, g.geometry);

            EXECUTE format('INSERT INTO fhnpi_faj_qgrids ("row_id", "original", "etrs10", "utm10", "centroid") VALUES (%L,%L,%L,%L,%L);', new.obm_id, geom, etrs10, utm10, centr );

            RETURN new; 

        ELSIF tg_op = 'UPDATE' AND (ST_Equals(OLD."obm_geometry", NEW."obm_geometry") = FALSE) THEN

            geom := new."obm_geometry";
	    centr := ST_Centroid(geom);

            SELECT geometry INTO etrs10 FROM shared.etrs10 g WHERE ST_Intersects(centr, g.geometry);
            SELECT geometry INTO utm10 FROM shared.utm10 g WHERE ST_Intersects(centr, g.geometry);

            execute format('UPDATE fhnpi_faj_qgrids set "original" = %L, "etrs10" = %L, utm10 = %L, "centroid" = %L WHERE row_id = %L;', geom, etrs10, utm10, centr, new.obm_id );

            RETURN new; 

        ELSIF tg_op = 'DELETE' THEN
            EXECUTE format('DELETE FROM fhnpi_faj_qgrids WHERE row_id = %L;', old.obm_id);

            RETURN old;
        
        ELSE
            RETURN new;
        
        END IF;
END; 
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
