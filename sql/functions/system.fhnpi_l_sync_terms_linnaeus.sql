CREATE OR REPLACE FUNCTION "fhnpi_l_sync_terms_linnaeus" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE 
    columns record;
    cmd character varying;
BEGIN
    IF (TG_OP = 'UPDATE' AND OLD.data_table = 'fhnpi_faj') THEN
        EXECUTE FORMAT('UPDATE system.%s_linnaeus SET updated_at = NOW() WHERE ARRAY[%L]::integer[] <@ %s;', OLD.data_table, OLD.term_id, OLD.subject || '_ids');
        RETURN NEW;
    ELSIF (TG_OP = 'DELETE' AND OLD.data_table = 'fhnpi_faj') THEN
        EXECUTE FORMAT('UPDATE system.%s_linnaeus SET updated_at = NOW() WHERE ARRAY[%L]::integer[] <@ %s;', OLD.data_table, OLD.term_id, OLD.subject || '_ids');
        RETURN OLD;
    END IF;
    RETURN NULL;
END;
$$