CREATE OR REPLACE FUNCTION "update_fhnpi_observerlist" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
gyujto RECORD;
tbl varchar(64) = TG_TABLE_NAME;
project varchar(5) = 'fhnpi';
gyid integer;
gyidk integer[] DEFAULT '{}';
akid integer;
hatid integer;
BEGIN
    IF tg_op = 'INSERT' THEN

        FOR gyujto IN (SELECT trim( unnest( regexp_split_to_array( concat_ws(',', NEW.mi_gyujto1, NEW.mi_gyujto2, NEW.mi_gyujto3),E'[,;]\\s*') ) ) as o) LOOP 
            SELECT add_term(gyujto.o, tbl, 'gyujto', project) INTO gyid;
            gyidk := gyidk || gyid;
        END LOOP;
        
        SELECT add_term(NEW.mi_adatkozlo, tbl, 'adatkozlo', project) INTO akid;
        SELECT add_term(NEW.mi_hatarozo, tbl, 'hatarozo', project) INTO hatid;


        EXECUTE format('INSERT INTO fhnpi_search_connect (data_table, row_id, gyujto_ids, adatkozlo_ids, hatarozo_ids) VALUES (%1$L, %2$L, %3$L, ''{%4$s}'', ''{%5$s}'');', tbl, NEW.obm_id, gyidk, akid, hatid );

        RETURN NEW;


    ELSIF tg_op = 'UPDATE' THEN

        IF  (OLD.mi_gyujto1 != NEW.mi_gyujto1) OR (OLD.mi_gyujto2 != NEW.mi_gyujto2) OR (OLD.mi_gyujto3 != NEW.mi_gyujto3) THEN
            FOR gyujto IN (SELECT trim( unnest( regexp_split_to_array( concat_ws(',', NEW.mi_gyujto1, NEW.mi_gyujto2, NEW.mi_gyujto3),E'[,;]\\s*') ) ) as n) LOOP 
                SELECT add_term(gyujto.n, tbl, 'gyujto', project) INTO gyid;
                gyidk := gyidk || gyid;
            END LOOP;

            EXECUTE format('UPDATE fhnpi_search_connect SET gyujto_ids = %2$L WHERE data_table = %1$L AND row_id = %3$L;', tbl, gyidk, NEW.obm_id);

        END IF;

        IF (OLD.mi_adatkozlo != NEW.mi_adatkozlo) THEN
            SELECT add_term(NEW.mi_adatkozlo, tbl, 'adatkozlo', project) INTO akid;
            EXECUTE format('UPDATE fhnpi_search_connect SET adatkozlo_ids = ''{%2$s}'' WHERE data_table = %1$L AND row_id = %3$L;', tbl, akid, NEW.obm_id);
        END IF;

        IF (OLD.mi_hatarozo != NEW.mi_hatarozo) THEN
            SELECT add_term(NEW.mi_hatarozo, tbl, 'hatarozo', project) INTO hatid;
            EXECUTE format('UPDATE fhnpi_search_connect SET hatarozo_ids = ''{%2$s}'' WHERE data_table = %1$L AND row_id = %3$L;', tbl, hatid, NEW.obm_id);
        END IF;
            

        RETURN NEW;

    ELSIF tg_op = 'DELETE' THEN

        RETURN OLD;

    END IF;                                                                                         
END
$$;


