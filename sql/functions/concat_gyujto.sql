CREATE OR REPLACE FUNCTION "concat_gyujto" () RETURNS trigger LANGUAGE plpgsql AS $$
DECLARE
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        NEW.mi_gyujto := concat_ws(',', NEW.mi_gyujto1, NEW.mi_gyujto2, NEW.mi_gyujto3);
        RETURN NEW;
    END IF;
    RETURN NULL;
END
$$;
