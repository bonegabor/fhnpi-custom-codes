CREATE OR REPLACE FUNCTION "add_term" ("term" character varying(128), "tbl" character varying(64), "subj" character varying(128), "project" character varying(64) ) RETURNS integer LANGUAGE plpgsql AS $$
DECLARE
sid integer;
BEGIN
    IF term IS NOT NULL AND tbl IS NOT NULL AND subj IS NOT NULL THEN
        EXECUTE format('SELECT search_id FROM %s_search ms WHERE "word" = %L AND "subject" = %L AND "data_table" = %L;', project, term, subj, tbl) INTO sid;
        IF sid IS NULL THEN
            EXECUTE format('INSERT INTO %4$s_search (data_table,subject,word,taxon_db) SELECT %2$L,%3$L,%1$L,1 RETURNING search_id;',term, tbl, subj, project) INTO sid;
        END IF;
        RETURN sid;
    END IF;
    RETURN NULL;
END;
$$;
