CREATE OR REPLACE FUNCTION "public"."fhnpi_general_rules_f" () RETURNS trigger AS
$$

DECLARE

 project varchar;
 tbl varchar;
 sens integer DEFAULT 0;

BEGIN

    project := 'fhnpi';

    IF TG_NARGS = 1 THEN
        IF TG_ARGV[0] = '' THEN
            tbl:= project;
        ELSE
            tbl := project || '_' || TG_ARGV[0];
        END IF;

        IF tg_op = 'INSERT' THEN

            sens := new."obm_access";


            execute format('INSERT INTO %1$s_rules ("data_table","row_id","write","read","sensitivity")
                            SELECT ''%2$s'',%3$L,ARRAY[uploader_id],%4$L,%5$L FROM system.uploadings WHERE id = %6$L',project, tbl, new.obm_id, (SELECT ARRAY(SELECT DISTINCT e FROM unnest(ARRAY[uploader_id, 26] || "group") as a(e)) FROM system.uploadings WHERE id = new.obm_uploading_id), sens, new.obm_uploading_id);

            RETURN new;

        ELSIF tg_op = 'UPDATE' AND OLD."obm_access" != NEW."obm_access" THEN

            sens := new."obm_access";

            execute format('UPDATE %1$s_rules SET sensitivity = %2$L WHERE data_table = ''%3$s'' AND row_id = %4$L',project, sens, tbl, NEW.obm_id);

            RETURN new;

        ELSIF tg_op = 'DELETE' THEN

            execute format('DELETE FROM %1$s_rules WHERE data_table = ''%2$s'' AND row_id = %3$L',project, tbl, old.obm_id);
            RETURN old;

        ELSE

            RETURN new;

        END IF;

    ELSE
        RETURN NULL;
    END IF;

END;
$$ LANGUAGE "plpgsql" COST 100
VOLATILE
CALLED ON NULL INPUT
SECURITY INVOKER;
