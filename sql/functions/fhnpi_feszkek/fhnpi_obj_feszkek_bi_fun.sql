CREATE OR REPLACE FUNCTION fhnpi_obj_feszkek_bi_fun()
RETURNS TRIGGER AS $$
DECLARE
    eov geometry;
    accuracy numeric;
    telepules varchar;
    erdoreszlet varchar;
BEGIN
    IF tg_op = 'INSERT' THEN
        -- Az 'azonosito' oszlop kitöltése a fajnév alapján, ha az még üres
        IF NEW.fr_azonosito IS NULL THEN
            NEW.fr_azonosito := generate_fr_azonosito(NEW.feszek_faj);
        END IF;
        
        eov := ST_Transform(ST_Centroid(NEW.obm_geometry), 23700);
        NEW.fr_eov_x := ST_X(eov);
        NEW.fr_eov_y := ST_Y(eov);

        SELECT metadata->>'gps_accuracy' INTO accuracy from system.uploadings WHERE id = new.obm_uploading_id;

        SELECT nev INTO telepules FROM fhnpi_hun_alt.fhnpi_hun_alt_kozigazgatas_telepules WHERE ST_Intersects(geom, eov);
        NEW.fr_telepules := telepules;

        SELECT gistext INTO erdoreszlet FROM fhnpi_local_nyilvtar.fhnpi_local_nyilvtar_erdoresz WHERE ST_Intersects(geom, eov);
        NEW.fr_erdoreszlet := erdoreszlet;

        IF NEW.feszek_lakott = 'igen' THEN
            NEW.feszek_utfeszk = extract(year from new.fr_date);
        END IF;

    END IF;

    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER fhnpi_obj_feszkek_bi_tr
BEFORE INSERT ON fhnpi_obj_feszkek
FOR EACH ROW
EXECUTE FUNCTION fhnpi_obj_feszkek_bi_fun();