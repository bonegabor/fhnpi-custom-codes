CREATE OR REPLACE FUNCTION fhnpi_obj_feszek_esemeny_ai_fun()
RETURNS TRIGGER AS $$
DECLARE
    update_query TEXT;
    first_field BOOLEAN := TRUE;
    feszek_connid varchar;
BEGIN
    IF tg_op = 'INSERT' and new.esemeny = 'módosítás' THEN
        -- Alap update parancs kezdete
        update_query := 'UPDATE fhnpi_obj_feszkek SET ';

        -- Ellenőrizzük az egyes mezők értékét
        IF NEW.es_feszek_faj IS NOT NULL THEN
            update_query := update_query || (CASE WHEN NOT first_field THEN ', ' ELSE '' END) || 'feszek_faj = ' || quote_literal(NEW.es_feszek_faj);
            first_field := FALSE;
        END IF;

        IF NEW.es_feszek_lakott IS NOT NULL THEN
            update_query := update_query || (CASE WHEN NOT first_field THEN ', ' ELSE '' END) || 'feszek_lakott = ' || quote_literal(NEW.es_feszek_lakott);
            first_field := FALSE;
        END IF;

        IF NEW.es_feszek_allapot IS NOT NULL THEN
            update_query := update_query || (CASE WHEN NOT first_field THEN ', ' ELSE '' END) || 'feszek_allapot = ' || quote_literal(NEW.es_feszek_allapot);
            first_field := FALSE;
        END IF;

        IF NEW.es_feszek_megjegyzes IS NOT NULL THEN
            update_query := update_query || (CASE WHEN NOT first_field THEN ', ' ELSE '' END) || 'megjegyzes = concat_ws('' / '', megjegyzes, ' || quote_literal(NEW.es_feszek_megjegyzes) || ')';
            first_field := FALSE;
        END IF;

        IF NEW.obm_files_id IS NOT NULL THEN
            SELECT obm_files_id INTO feszek_connid FROM fhnpi_obj_feszkek WHERE fr_azonosito = new.fr_azonosito;

            IF feszek_connid IS NULL THEN
                update_query := update_query || (CASE WHEN NOT first_field THEN ', ' ELSE '' END) || 'obm_files_id = ' || quote_literal(NEW.obm_files_id);
                first_field := FALSE;
            ELSE
                INSERT INTO file_connect (file_id, conid, temporal, sessionid, rownum) SELECT file_id, feszek_connid, temporal, sessionid, rownum FROM file_connect WHERE conid = new.obm_files_id;
            END IF;
        END IF;

        IF first_field THEN
            RETURN NEW;
        END IF;

        -- Az UPDATE WHERE részének befejezése
        update_query := update_query || ' WHERE fr_azonosito = ' || quote_literal(NEW.fr_azonosito);

        -- Frissítés végrehajtása
        EXECUTE update_query;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;


-- CREATE TRIGGER fhnpi_obj_feszek_esemeny_ai_tr
-- AFTER INSERT ON fhnpi_obj_feszek_esemeny
-- FOR EACH ROW
-- EXECUTE FUNCTION fhnpi_obj_feszek_esemeny_ai_fun();

 