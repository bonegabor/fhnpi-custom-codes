CREATE OR REPLACE FUNCTION fhnpi_obj_feszkek_ai_fun()
RETURNS TRIGGER AS $$
BEGIN
    IF tg_op = 'INSERT' THEN
        INSERT INTO fhnpi_obj_feszek_esemeny (fr_azonosito, esemeny, es_datum, es_szemely, es_feszek_faj, es_feszek_lakott, es_feszek_allapot, es_feszek_megjegyzes)
        VALUES (new.fr_azonosito, 'új fészek rögzítése', new.fr_date, new.fr_szemely, new.feszek_faj, new.feszek_lakott, new.feszek_allapot, new.megjegyzes);
        RETURN NEW;
    END IF;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER fhnpi_obj_feszkek_ai_tr
AFTER INSERT ON fhnpi_obj_feszkek
FOR EACH ROW
EXECUTE FUNCTION fhnpi_obj_feszkek_ai_fun();