CREATE OR REPLACE FUNCTION generate_fr_azonosito(fajnev TEXT)
RETURNS TEXT AS $$
DECLARE
    -- Egyedi rövidítés előre definiált lista alapján
    faj_short TEXT;
    max_sorsz INT;
    new_id TEXT;

    -- Fajok rövidítési listája
    faj_rovidites CONSTANT JSONB := '{
        "barna kánya": "bkan",
        "békászósas": "bsas",
        "darázsölyv": "doly",
        "dolmányos varjú": "dvarj",
        "egerészölyv": "eoly",
        "fehér gólya": "fehgo",
        "feketególya": "fekgo",
        "gyöngybagoly": "gyb",
        "héja": "heja",
        "holló": "hollo",
        "kabasólyom": "kaba",
        "karvaly": "karv",
        "kékvércse": "kv",
        "kerecsensólyom": "kerec",
        "macskabagoly": "macsb",
        "nem ismert": "nemism",
        "parlagisas": "prsas",
        "rétisas": "rsas",
        "uhu": "uhu",
        "vándorsólyom": "vands",
        "vörös kánya": "vkan",
        "vörösvércse": "vv"
    }'::JSONB;

BEGIN
    -- Rövidítés keresése a fajnév alapján
    faj_short := faj_rovidites ->> fajnev;

    -- Ha a fajnév nem található a listában, hibaüzenet
    IF faj_short IS NULL THEN
        RAISE EXCEPTION 'Ismeretlen fajnév: %', fajnev;
    END IF;

    -- Legnagyobb sorszám lekérdezése
    SELECT COALESCE(MAX(SPLIT_PART(fr_azonosito, '_', 3)::INT), 0)
    INTO max_sorsz
    FROM fhnpi_obj_feszkek
    WHERE fr_azonosito SIMILAR TO 'fh_' || faj_short || '_[0-9]{3}';

    -- Új azonosító létrehozása
    new_id := 'fh_' || faj_short || '_' || LPAD((max_sorsz + 1)::TEXT, 3, '0');

    RETURN new_id;
END;
$$ LANGUAGE plpgsql;
