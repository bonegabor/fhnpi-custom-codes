CREATE TRIGGER "fhnpi_taxon_meta_fill" BEFORE INSERT OR UPDATE ON "fhnpi_taxon" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
CREATE TRIGGER "fhnpi_search_meta_fill" BEFORE INSERT OR UPDATE ON "fhnpi_search" FOR EACH ROW EXECUTE PROCEDURE fill_search_meta();
