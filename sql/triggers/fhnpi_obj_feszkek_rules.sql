CREATE TRIGGER fhnpi_obj_feszkek_rules BEFORE INSERT or UPDATE OR DELETE ON public.fhnpi_obj_feszkek FOR EACH ROW EXECUTE PROCEDURE public.fhnpi_general_rules_f('obj_feszkek');
CREATE TRIGGER fhnpi_obj_feszek_esemeny_rules BEFORE INSERT or UPDATE OR DELETE ON public.fhnpi_obj_feszek_esemeny FOR EACH ROW EXECUTE PROCEDURE public.fhnpi_general_rules_f('obj_feszek_esemeny');
