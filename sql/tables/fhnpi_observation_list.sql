-- Adminer 4.6.3 PostgreSQL dump

DROP TABLE IF EXISTS "fhnpi_observation_list";
DROP SEQUENCE IF EXISTS fhnpi_observation_id_seq;
CREATE SEQUENCE fhnpi_observation_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE "public"."fhnpi_observation_list" (
    "id" integer DEFAULT nextval('fhnpi_observation_id_seq') NOT NULL,
    "oidl" character varying(40),
    "uploading_id" integer,
    "obsstart" timestamp,
    "obsend" timestamp,
    "nulllist" boolean,
    CONSTRAINT "fhnpi_observation_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

GRANT ALL ON fhnpi_observation_list TO fhnpi_admin;
GRANT ALL ON fhnpi_observation_id_seq TO fhnpi_admin;
-- 2020-03-03 03:08:09.705009+02