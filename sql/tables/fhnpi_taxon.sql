    ALTER TABLE fhnpi_taxon
    ADD CONSTRAINT check_lang_when_status_accepted
    CHECK (status = 'accepted' AND lang IN ('mi_fajlatin', 'mi_fajmagyar'));
