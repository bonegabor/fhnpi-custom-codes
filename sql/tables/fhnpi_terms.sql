ALTER TABLE fhnpi_terms ADD COLUMN parent_id integer;
--ALTER TABLE fhnpi_terms ADD CONSTRAINT parent_id_fk FOREIGN KEY (parent_id) REFERENCES fhnpi_terms (term_id);

BEGIN;
WITH asdf as (
    select distinct 
    t1.term_id as parent_id, t1.term, t2.term, t2.term_id
    from fhnpi_terms t1 
    LEFT JOIN fhnpi_terms t2 ON t2.subject = 'objektum_tipus' AND t2.term LIKE t1.term || '%'
    where t1.subject = 'objektum_fo'
)
UPDATE fhnpi_terms  
SET parent_id = asdf.parent_id
FROM asdf
where subject = 'objektum_tipus' AND fhnpi_terms.term_id = asdf.term_id;

