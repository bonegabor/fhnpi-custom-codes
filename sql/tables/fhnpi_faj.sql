ALTER TABLE fhnpi_faj DROP COLUMN  mi_fajered_javitott;

ALTER TABLE temporary_tables.fhnpi_faj_obm_obsl ADD COLUMN mi_fajlatin character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_obsl ADD COLUMN mi_fajmagyar character varying NULL;

ALTER TABLE temporary_tables.fhnpi_faj_obm_upl ADD COLUMN mi_fajlatin character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_upl ADD COLUMN mi_fajmagyar character varying NULL;

ALTER TABLE fhnpi_faj ADD COLUMN obm_observation_list_id character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_obsl ADD COLUMN obm_observation_list_id character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_upl ADD COLUMN obm_observation_list_id character varying NULL;

ALTER TABLE fhnpi_faj ADD COLUMN mi_gyujto character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_obsl ADD COLUMN mi_gyujto character varying NULL;
ALTER TABLE temporary_tables.fhnpi_faj_obm_upl ADD COLUMN mi_gyujto character varying NULL;
