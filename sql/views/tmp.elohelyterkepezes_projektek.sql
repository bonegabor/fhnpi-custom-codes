CREATE OR REPLACE VIEW "tmp"."elohelyterkepezes_projektek" AS
SELECT pr_azonosito, pr_nev FROM fhnpi_metaadat WHERE pr_nev LIKE 'Élőhely%';

GRANT ALL ON tmp.elohelyterkepezes_projektek TO fhnpi_admin;