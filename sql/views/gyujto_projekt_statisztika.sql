CREATE VIEW gyujto_projekt_statisztika AS SELECT pr_alprojekt, mi_gyujto1, extract(year from fr_datum) as ev, count(*) FROM fhnpi_faj GROUP BY pr_alprojekt, mi_gyujto1, extract(year from fr_datum);

GRANT ALL ON gyujto_projekt_statisztika to pokasz_gmail_com ;
GRANT ALL ON gyujto_projekt_statisztika to fhnpi_admin ;