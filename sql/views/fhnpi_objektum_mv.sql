CREATE MATERIALIZED VIEW fhnpi_objektum_mv AS 
SELECT distinct 
    t1.term_id as fo_id, t1.term as objektum_fo, t2.term_id as tipus_id, t2.term as objektum_tipus
from fhnpi_terms t1 
LEFT JOIN fhnpi_terms t2 ON t2.subject = 'objektum_tipus' AND t1.term_id = t2.parent_id
where t1.subject = 'objektum_fo';
GRANT ALL ON fhnpi_objektum_mv TO fhnpi_admin;
