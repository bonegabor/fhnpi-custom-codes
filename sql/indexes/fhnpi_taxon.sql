CREATE INDEX fhnpi_taxon_meta_idx ON fhnpi_taxon USING btree (meta);
CREATE INDEX fhnpi_taxon_taxon_id ON fhnpi_taxon USING btree (taxon_id);
CREATE INDEX fhnpi_taxon_word_idx ON fhnpi_taxon USING btree (word);
CREATE UNIQUE INDEX fhnpi_taxon_word_lang_status ON fhnpi_taxon USING btree (word, lang, status);



