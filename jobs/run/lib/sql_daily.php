<?php
class sql_daily extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('sql_daily initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }
        if (!query($ID,$params)) {
            job_log('sql_daily query failed!');
            exit;
        }
        exit;
    }
}
?>
