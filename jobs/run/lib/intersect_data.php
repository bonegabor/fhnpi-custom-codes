<?php
class intersect_data extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params, $pa) {
        debug('intersect_data initialized', __FILE__, __LINE__);
        return true;
    }

    public function get_results() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        $cmdpart = [];
        $tblHeader = ['total rows'];
        $data_table = '';
        foreach ($params as $col => $opts) {
            $data_table = $opts->dataTable;
            $cmdpart[] = "count(CASE WHEN $col IS NOT NULL THEN 1 END) AS $col";
            $tblHeader[] = $col;
        }
        $cmd = sprintf("SELECT count(*) AS total, %s FROM %s_qgrids;", implode(', ', $cmdpart), $data_table);

        if (!$res = query($ID,$cmd))
            return 'ERROR: query error';


        $results = pg_fetch_assoc($res[0]);

        $t = [$results['total']];
        $tbl = new createTable();
        $tbl->def(['tid'=>__CLASS__.'-results-table','tclass'=>'resultstable']);
        $tbl->addHeader($tblHeader);

        foreach ($params as $col => $opts) {
            $t[] = $results[$col]. ' ('. round($results[$col] * 100 / $t[0],2) .'%)';
        }

        $tbl->addRows($t);

        return $tbl->printOut();
    }


    static function run() {
        global $ID;
        $params = parent::getJobParams(__CLASS__);
        $limit = round(1000 / count((array)$params));
        $cmd = [];
        foreach ($params as $col => $opts) {
            job_log("intersect_data: " . $col);
            $filter = (isset($opts->filterColumn) && isset($opts->filterValue)) 
                ? "g.{$opts->filterColumn} = '{$opts->filterValue}'" 
                : "1 = 1";

            $transform = (isset($opts->srid))
                ? sprintf("ST_Transform(rqg.centroid,%s)", $opts->srid)
                : "rqg.centroid";

            $cmd[] = sprintf('WITH rows AS ( SELECT rqg.row_id, g.%2$s FROM %9$s_qgrids rqg LEFT JOIN %3$s.%4$s g ON ST_Intersects(%6$s,%7$s) WHERE %1$s IS NULL AND %5$s ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = rows.%2$s FROM rows WHERE rows.row_id = qg.row_id;', 
                $col, 
                $opts->valueColumn, 
                $opts->schema, 
                $opts->table,
                $filter,
                $transform,
                $opts->geomColumn,
                $limit,
                $opts->dataTable
            );
            if ($opts->valueColumn !== 'geometry') {
                $cmd[] = sprintf('WITH rows AS ( SELECT rqg.row_id FROM %9$s_qgrids rqg WHERE %1$s IS NULL AND NOT EXISTS (SELECT 1 FROM %3$s.%4$s g WHERE ST_Intersects(%6$s,%7$s) AND %5$s) ORDER BY row_id DESC LIMIT %8$d) UPDATE %9$s_qgrids qg SET %1$s = \'NA\' FROM rows WHERE rows.row_id = qg.row_id;', 
                    $col, 
                    $opts->valueColumn, 
                    $opts->schema, 
                    $opts->table,
                    $filter,
                    $transform,
                    $opts->geomColumn,
                    $limit,
                    $opts->dataTable
                );
            }
        }
        query($ID, $cmd);
    }
}
?>
