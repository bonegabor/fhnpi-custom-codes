<?php
class observation_lists extends job_module {

    public function __construct($mtable) {
        parent::__construct(__CLASS__,$mtable);
    }

    public function init($params,$pa) {
        debug('observation_lists initialized', __FILE__, __LINE__);
        return true;
    }

    static function run() {
        global $ID;

        $params = parent::getJobParams(__CLASS__);
        if (!$params) {
            job_log('job parametes missing');
            return;
        }

        foreach ($params as $table => $options) {
            extract((array)$options);
            
            //check is the messages module is included
            if ( ! class_exists('Messenger')) {
                job_log('Messenger class not included!');
                return;
            }
            if ( ! class_exists('Role') ) {
                job_log('Messenger class not included!');
                return;
            }
            //preparing the time conversion sql 
            $time_conversion = "to_timestamp(%s::bigint/1000)";
            $duration_conversion = "($time_conversion - $time_conversion)::time";
            if (isset($only_time)) {
                $time_conversion .= "::time";
                if (isset($time_as_int)) {
                    $time_conversion = "extract(epoch from date_trunc('minute',$time_conversion))/60::integer";
                    $duration_conversion = "extract(epoch from date_trunc('minute',$duration_conversion))/60::integer";
                }
            }
            
            
            $tmptable = "{$table}_obm_obsl";
            $dst_columns = array_values(array_filter(self::getColumnsOfTables($table), function($col) { return $col != 'obm_id'; }));
            $src_columns = array_values(array_filter(self::getColumnsOfTables($tmptable, 'temporary_tables'), function($col) { return $col != 'obm_id'; }));
            $columns_intersected = array_intersect($src_columns, $dst_columns);
            
            if ((count($dst_columns) != count($src_columns)) || (count($dst_columns) != count($columns_intersected))) {
                $missing_columns = array_diff($dst_columns,$src_columns);
                if (count($missing_columns)) {
                    job_log("ERROR: Column(s) missing from $tmptable: " . implode(', ', $missing_columns));
                }
                $missing_columns = array_diff($src_columns,$dst_columns);
                if (count($missing_columns)) {
                    job_log("ERROR: Column(s) missing from $table: " . implode(', ', $missing_columns));
                }
                return;
            } 
            $common_columns = implode(',', $columns_intersected);
            //get the unprocessed lists with observations
            $lists = self::get_unprocessed_lists( $table );
            if ($lists === 'error') {
                job_log('observation_lists failed 2');
                return;
            }
            //get the unprocessed null lists
            $nlists = self::get_unprocessed_null_lists( $table );
            if ($nlists === 'error') {
                job_log('observation_lists failed 3');
                return;
            }

            $warnings = [];
            $lists = array_merge($lists, $nlists);
            foreach ($lists as $list) {
                $cmd = [];
                $update_cmd = [];
                $list_id_column = $list_id_column ?? 'obm_observation_list_id';

                if ($list['measurements_num'] != count(explode(',',$list['observation_list_elements']))) { //2 adat
                    continue;
                }

                $update_cmd[] = sprintf("%s = %s", $list_id_column, quote($list['observation_list_id']));
                if (isset($list_start_column)) {
                    if (!is_null($list['observation_list_start'])) {
                        $update_cmd[] = sprintf("%s = $time_conversion", $list_start_column, $list['observation_list_start']);
                    }
                    else {
                        $warnings[] = [
                            'observation_list_id' => $list['observation_list_id'],
                            'message' => 'observation_list_start missing'
                        ];
                    }
                }
                if (isset($list_end_column)) {
                    if (!is_null($list['observation_list_end'])) {
                        $update_cmd[] = sprintf("%s = $time_conversion", $list_end_column, $list['observation_list_end']);
                    }
                    else {
                        $warnings[] = [
                            'observation_list_id' => $list['observation_list_id'],
                            'message' => 'observation_list_end missing'
                        ];
                    }
                }
                if (isset($list_duration_column)) {
                    $cmd2 = sprintf("SELECT FROM temporary_tables.%2\$s WHERE obm_uploading_id IN (%3\$s) AND %1\$s IS NOT NULL;", $list_duration_column, $tmptable, $list['observation_list_elements']);
                    if (! $res = pg_query($ID, $cmd2)) {
                        job_log("$tmptable query error");
                        return;
                    }
                    if (! pg_num_rows($res) && !is_null($list['observation_list_end']) && !is_null($list['observation_list_start'])) {
                        $update_cmd[] = sprintf("%s = $duration_conversion", $list_duration_column, $list['observation_list_end'], $list['observation_list_start']);
                    }
                }

                $cmd[] = sprintf("UPDATE temporary_tables.%s SET %s WHERE obm_uploading_id IN (%s);", $tmptable, implode(', ',$update_cmd), $list['observation_list_elements']);

                $cmd[] = sprintf("INSERT INTO %s (%s) SELECT %s FROM temporary_tables.%s WHERE obm_uploading_id IN (%s);",
                            $table,
                            $common_columns,
                            $common_columns,
                            $tmptable,
                            $list['observation_list_elements']
                        );
                $cmd[] = sprintf("INSERT INTO %s_observation_list (oidl, uploading_id, obsstart, obsend) VALUES (%s, %s, %s, %s);",
                            PROJECTTABLE,
                            quote($list['observation_list_id']),
                            quote($list['uploading_id']),
                            (is_null($list['observation_list_start'])) ? 'NULL' : "to_timestamp({$list['observation_list_start']}::bigint/1000)",
                            (is_null($list['observation_list_end'])) ? 'NULL' : "to_timestamp({$list['observation_list_end']}::bigint/1000)",
                );
                $cmd[] = sprintf("UPDATE system.uploadings SET project_table = %s WHERE id IN (%s);", quote($table), $list['observation_list_elements']);
                $cmd[] = sprintf("UPDATE system.uploadings SET project_table = %s WHERE id = %s;", quote($table), quote($list['uploading_id']));

                $cmd[] = sprintf("DELETE FROM temporary_tables.%s WHERE obm_uploading_id IN (%s);", $tmptable, $list['observation_list_elements']);

                if ( query($ID, $cmd) ) {
                    job_log($list['observation_list_id'] . ': list processed');
                }
            }
            
            if (count($warnings) && isset($mail_to)) {
                
                $warning_table_rows = implode("", array_map(function($w) {
                    return "<tr><td>{$w['observation_list_id']}</td><td>{$w['message']}</td></tr>";
                }, $warnings));
                debug($warnings_table_rows,__FILE__,__LINE__);
                $m = new Messenger();
                $role = new Role($mail_to);
                
                foreach ($role->get_members() as $to) {
                    $words = [
                        'name' => $to->name,
                        'warning_table_rows' => $warning_table_rows
                    ];
                    
                    $m->send_system_message($to, 'list_processing_warning', $words, true);
                    
                }
            }
        }
    }

    public static function get_unprocessed_lists($table) {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table = 'temporary_tables.%2\$s_obm_obsl')
                        SELECT
                            l.id AS uploading_id,
                            l.uploader_id,
                            l.uploader_name,
                            l.uploading_date,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            string_agg(o.id::text,',') as observation_list_elements
                        FROM upl l LEFT JOIN upl o ON l.metadata->>'observation_list_id' = o.metadata->>'observation_list_id'
                        WHERE
                            l.metadata::jsonb ? 'measurements_num' AND
                            NOT o.metadata::jsonb ? 'measurements_num'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num, l.uploader_id, l.uploader_name, l.uploading_date
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        $table
                    );
        if (! $res = query($ID, $cmd) ) {
            return 'error';
        }
        $results = pg_fetch_all($res[0]);
        return ($results) ? $results : [];
    }
    
    public static function get_unprocessed_null_lists($table) {
        global $ID;
        $cmd = sprintf("WITH upl AS (SELECT * FROM system.uploadings WHERE
                            project = '%1\$s' AND
                            project_table = 'temporary_tables.%2\$s_obm_obsl')
                        SELECT
                            l.id AS uploading_id,
                            l.uploader_id,
                            l.uploader_name,
                            l.uploading_date,
                            l.metadata->>'observation_list_id' as observation_list_id,
                            l.metadata->>'observation_list_start' as observation_list_start,
                            l.metadata->>'observation_list_end' as observation_list_end,
                            l.metadata->>'observation_list_null_record' as observation_list_null_record,
                            l.metadata->>'measurements_num' as measurements_num,
                            l.id::text as observation_list_elements
                        FROM upl l 
                        WHERE
                            l.metadata::jsonb ? 'observation_list_null_record' AND 
                            l.metadata->>'observation_list_null_record' = 'true'
                        GROUP BY l.id, observation_list_id, observation_list_start, observation_list_end, observation_list_null_record, measurements_num, l.uploader_id, l.uploader_name, l.uploading_date
                        ORDER BY uploading_id;",
                        PROJECTTABLE,
                        $table
                    );
        if (! $res = query($ID, $cmd) ) {
            return 'error';
        }
        $results = pg_fetch_all($res[0]);
        return ($results) ? $results : [];
    }

    private static function getColumnsOfTables($table=PROJECTTABLE,$schema='public') {
        global $GID;
        if ($schema == '') $schema = 'public';
        if ($table == '') $table = PROJECTTABLE;
        $cmd = sprintf("SELECT column_name FROM information_schema.columns WHERE table_name=%s AND table_schema=%s ORDER BY column_name",quote($table),quote($schema));
        $result = pg_query($GID,$cmd);
        $columns = array();
        while($row=pg_fetch_assoc($result)) {
            $columns[] = $row['column_name'];
        }
        return $columns;
    }
}
?>
